#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import random
import scipy.sparse as sps
from probabilita import Probability
from sqlalchemy import create_engine
from sqlalchemy.sql import select


USER = "A3KROYMQ61M7A"

# connessione al db
engine = create_engine('sqlite:///csv_database.db')
connection = engine.connect()
# query per prendere i dati dal db
all_items = connection.execute("SELECT codItem FROM items i where asin not in (select asin from ratings where user='{}')".format(USER))

prob = Probability()
mtx = sps.load_npz("matrix_filtered.npz")
mtx = mtx.tocsr()
for i in all_items:
    print(prob.naive_bayes(7, i[0], mtx))