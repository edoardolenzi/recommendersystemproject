import numpy as np
import scipy.sparse as sps


class Probability():


    def __init__(self):
        print("Probability..")


    def naive_bayes(self, user, index, mtx):
        py = self.__py(mtx[:,index].toarray())
        py_x = self.__py_x(user, index, mtx)
        data = []
        for i in range (1,6):
            data.append(py[i] * py_x[i])
        print(data)
        return data
    

    def __get_every_rating(self, user, index, mtx):
        col = np.shape(mtx)[1]
        all_counter = [0]*6
        check_matrix = [[0]*col]*6
        data = []
        for i in mtx[user,:].nonzero()[1]:
            for j in mtx[:,i].nonzero()[0]:
                if i != index and mtx[j,i] == mtx[user,i] and mtx[j,index] != 0:
                    check_matrix[mtx[j,index]][i] += 1

        for i in mtx[:,index].nonzero()[0]:
            all_counter[mtx[i,index]] += 1

        for i in range(0,6):
            temp = 1
            for j in range(0,col):
                if check_matrix[i][j] != 0:
                    temp *= (check_matrix[i][j] / all_counter[i])
            data.append(temp if temp != 1 else 0)

        return data


    def __py_x(self, user, index, mtx):
        every_rating = self.__get_every_rating(user,index,mtx)
        return every_rating


    def __py(self, mtx):
        counter = [0]*6
        for rating in mtx:
            counter[rating[0]] += 1
        sum_counter = sum(counter)
        prob = [count / sum_counter for count in counter]
        return prob
