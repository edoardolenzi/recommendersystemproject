#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Sparse matrices handler'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import logging
import os
import numpy as np
import scipy.sparse as sps
from scipy.io import mmio as io

from rsproject.commons import constants as const
from rsproject.commons.db_manager import DBManager
from rsproject.decorators.singleton import singleton
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix

@singleton
@for_all_methods(timer)
class SparseMatrix():


    '''
    Sparse matrices handler, the idea is to share some sparse matrix instances 
    exploiting the @singleton decorator 
    '''

    __mtx = None
    __matrices = None
    __db_manager = None
    LOGGER = logging.getLogger(__name__)
    

    def __init__(self):
        self.__matrices = dict()
        self.__db_manager = DBManager()
        self.LOGGER.info('Connesso al db')


    '''
    Creates a sparse matrix |users| x |items| with the ratings. 
    The matrix can be complete or filtered by user (see environment definitions)    

    Args:
        ratings_table: name of the ratings table (in the DB)
        users_table: name of the users table (in the DB)
        items_table: name of items table (in the DB)
    '''

    def create(self, ratings_table: str, users_table: str, items_table: str) -> None:
        ratings_query = 'SELECT user, item, rating FROM {0}'.format(ratings_table)
        users_query = 'SELECT count(*) FROM {0}'.format(users_table)
        items_query = 'SELECT count(*) FROM {0}'.format(items_table)

        total_users = self.__db_manager.execute(users_query).scalar()
        total_items = self.__db_manager.execute(items_query).scalar()
        ratings = self.__db_manager.execute(ratings_query)

        self.LOGGER.debug('Presi tutti i rating')
        self.LOGGER.debug('Colonne totali: {}'.format(total_users))
        self.LOGGER.debug('Righe totali: {}'.format(total_items))

        # creazione della matrice
        self.__mtx = sps.lil_matrix((total_users, total_items), dtype=np.int)
        self.LOGGER.info('Creata la matrice in memory')

        # inserimento dei valori nella matrice
        for (user, item, rating) in ratings:
            try:
                self.__mtx[user, item] = rating
            except:
                self.LOGGER.warning('Error on user {0}, item {1}'.format(user, item))


    '''
    Exports the matrix to an .mtx file    

    Args:
        matrix_path: path of the output file
    '''

    def export_matrix(self, matrix: Matrix = Matrix(None, const.SPARSE_MATRIX_PATH)) -> None:
        #sps.save_npz(const.SPARSE_MATRIX_PATH, self.__mtx)
        try:
            os.remove(matrix.matrix_path)
        except:
            pass
        io.mmwrite(matrix.matrix_path, self.__mtx if matrix.matrix is None else matrix.matrix)
        if self.__mtx is not None:
            del self.__mtx  
        self.LOGGER.info('Matrice esportata e deallocata')


    '''
    Given an instance of Matrix, returns an instance of Matrix in every case.
    The class is decorated with @singleton so there is a shared list of matrix instances

    Args:
        matrix: is a Matrix instance, can be like:
            * Matrix(matrix = None, matrix_path = 'matrix_path')        loads the file on path: matrix_path
            * Matrix(matrix = CsrMatrix, matrix_path = 'matrix_name')   save in the list of matrices the CsrMatrix 

    Returns:
        Matrix instance which wraps a csr matrix (Compressed Sparse Row matrix)
    '''

    def import_matrix(self, matrix: Matrix = Matrix(None, const.SPARSE_MATRIX_PATH)) -> object:
        # mat = sps.load_npz(const.SPARSE_MATRIX_PATH)
        # self.LOGGER.debug(mat.todense())
        if matrix.matrix is not None:
            if matrix.matrix_path not in self.__matrices:
                self.__matrices[matrix.matrix_path] = matrix 
            return self.__matrices[matrix.matrix_path]
        elif matrix.matrix_path not in self.__matrices:
            self.__matrices[matrix.matrix_path] = Matrix(io.mmread(matrix.matrix_path).tocsr(), matrix.matrix_path)
            self.LOGGER.info('Matrice importata')
        return self.__matrices[matrix.matrix_path]
        
        
    '''
    Converts a matrix into a csr matrix

    Args:
        matrix:     the input matrix

    Returns:
        A csr sparse matrix 
    '''

    def to_csr(self, matrix: []) -> object:
        csr_mtx = sps.lil_matrix((np.shape(matrix)[0], np.shape(matrix)[1]), dtype=np.float)
        for i, row in enumerate(matrix):
            for j, item in enumerate(row): 
                if item != 0:
                    csr_mtx[i][j] = item
        return csr_mtx