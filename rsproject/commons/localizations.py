#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Logging facility'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import os

# Exceptions messages
FAIL_CREATE_ENGINE = 'Fails to create DB engine'
MISSING_DATA_KEY = 'Missing DATA declaration in the environment'