#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Base SQL Alchemy ORM entity'''

__author__ = 'Edoardo Lenzi'
__email__ = 'edoardolenzi9@gmail.com'
__version__ = '1.0'
__license__ = 'GPL-3.0'
__copyright__ = 'Copyleft 2018, lenzi.edoardo'

from sqlalchemy import Column, Date, Index, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

from rsproject.commons.models.base_entity import BaseEntity

Base = declarative_base()


class DocumentClusterEntity(BaseEntity, Base):
    __tablename__ = 'document_clusters'

    document = Column(Integer, nullable=False)
    cluster = Column(Integer, nullable=False)


class UserClusterEntity(BaseEntity, Base):
    __tablename__ = 'user_clusters'

    user = Column(Integer, nullable=False)
    cluster = Column(Integer, nullable=False)