#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Base SQL Alchemy ORM entity'''

__author__ = 'Edoardo Lenzi'
__email__ = 'edoardolenzi9@gmail.com'
__version__ = '1.0'
__license__ = 'GPL-3.0'
__copyright__ = 'Copyleft 2018, lenzi.edoardo'

class Matrix():

    matrix = None
    matrix_path = None

    def __init__(self, matrix, matrix_path):
        self.matrix = matrix
        self.matrix_path = matrix_path