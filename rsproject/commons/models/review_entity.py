#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Base SQL Alchemy ORM entity'''

__author__ = 'Edoardo Lenzi'
__email__ = 'edoardolenzi9@gmail.com'
__version__ = '1.0'
__license__ = 'GPL-3.0'
__copyright__ = 'Copyleft 2018, lenzi.edoardo'

from sqlalchemy import Column, Date, Index, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

from rsproject.commons.models.base_entity import BaseEntity

Base = declarative_base()


class ReviewEntity(BaseEntity, Base):
    __tablename__ = 'reviews'

    user = Column(Integer, nullable=False)
    userName = Column(String(49))
    item = Column(Integer, nullable=False)
    rating = Column(Float, nullable=False)
    review = Column(Integer, nullable=False)
    reviewText = Column(String(32766))
    summary = Column(String(251))
    overall = Column(Float)
    helpful = Column(Integer)
    reviewTime = Column(String(11))
    unixReviewTime = Column(Integer)