#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Base SQL Alchemy ORM entity'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from sqlalchemy import Column, Date, Index, Integer, String


class BaseEntity():
    __table_args__ = {'mysql_charset': 'utf8mb4'}
    id = Column(Integer, unique=True,
                         primary_key=True, autoincrement=True)

    def __repr__(self) -> str:
        return "<BaseEntity(id='{0}')>".format(self.id)
