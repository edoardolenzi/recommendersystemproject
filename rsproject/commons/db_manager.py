#!/usr/bin/env python3
# coding: utf-8

'''DB manager based on SQL Alchemy engine'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import json
import logging
import os
import re
from sqlalchemy import create_engine
from sqlalchemy.engine import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.pool import NullPool

from rsproject.commons import constants as const
from rsproject.commons import localizations as loc

BASE = declarative_base()
LOGGER = logging.getLogger(__name__)

class DBManager():

    '''
    Exposes some primitives for the DB access;
    note that some methods only work with a SQLite DB
    '''


    __engine = None
    __connection = None

    def __init__(self):
        with open(const.DB_CREDENTIALS_PATH) as f:
            credentials = json.load(f)

        db_engine = credentials[const.DB_ENGINE_KEY]
        db_name = credentials[const.PROD_DB_KEY]
        user = credentials[const.USER_KEY]
        password = credentials[const.PASSWORD_KEY]
        host = credentials[const.HOST_KEY]

        try:
            # Disable connection pooling, as per Wikimedia policy
            # https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Connection_handling_policy
            if const.env('DB_CONTEXT') == 'SQL':
                self.__engine = create_engine(
                    '{0}://{1}:{2}@{3}/{4}'.format(db_engine, user, password, host, db_name), poolclass=NullPool)
            elif const.env('DB_CONTEXT') == 'SQLITE':
                self.__engine = create_engine(const.SQLITE_DB)
            else:
                raise Exception('Missing DB_CONTEXT declaration in .env file')
        except Exception as error:
            LOGGER.critical(loc.FAIL_CREATE_ENGINE, error)
        self.__connection = self.get_new_connection()


    '''
    Creates a new DB connection
    '''

    def get_new_connection(self) -> object:
        return self.__engine.connect()


    '''
    Returns the DB connection
    '''
    
    def get_connection(self) -> object:
        return self.__connection


    '''
    Returns the current SQL Alchemy engine instance
    '''

    def get_engine(self) -> Engine:
        return self.__engine


    '''
    Returns a new SQL Alchemy session
    '''

    def new_session(self) -> object:
        Session = sessionmaker(bind=self.__engine)
        return Session()


    '''
    Returns a new SQL Alchemy scoped session
    '''

    def new_scoped_session(self) -> object:
        DBSession = scoped_session(sessionmaker())
        DBSession.remove()
        DBSession.configure(bind=self.__engine, autoflush=False, expire_on_commit=False)
        return DBSession


    '''
    Creates the tables (tables can be ORM entity instances or classes)
    '''
    
    def create(self, tables) -> None:
        BASE.metadata.create_all(self.__engine, tables=[
                                 table.__table__ for table in tables])


    '''
    Drops the tables (table can be ORM entity instances or classes)
    '''

    def drop(self, tables) -> None:
        BASE.metadata.drop_all(self.__engine, tables=[
                               table.__table__ for table in tables])


    '''
    Executes a query, if the query contains a 'create table' statement then 
    drops that table or do nothing, depending on the value of 
    OVERRIDE_TABLE environment variable

    Args:
        query: the query to be executed 

    Returns:
        the query result
    '''

    def execute(self, query: str) -> object:
        # checks if the table already exists
        if 'create table' in query or 'CREATE TABLE' in query:
            match = re.search('(?<=create table )[^ ]*', query.lower())
            if match is not None:
                if const.env('OVERRIDE_TABLE') == 'FALSE':
                    if self.__connection.execute('''SELECT count(*) 
                                                    FROM sqlite_master 
                                                    WHERE type='table' and name = '{0}' '''.format(match.group(0))).scalar() > 0:
                        return ''
                else:
                    try:
                        self.execute('DROP TABLE {0}'.format(match.group(0)))
                    except:
                        pass
        return self.__connection.execute(query)


    '''
    Materializes a query to a table 

    Args:
        table_name: the name of the new table
        query: given query
    '''

    def materialize(self, table_name: str, query: str) -> None:
        self.__connection.execute('create table {0} as {1}'.format(table_name, query))
        self.rename_and_index_table(table_name, 'select * from {0}'.format(table_name))


    '''
    Creates/rename a table and adds a unique column 'id' autoincremental (starts from 0)

    Args:
        table_name: the new name for the table
        query: given query
    '''

    def rename_and_index_table(self, table_name: str, query: str) -> None:
        self.__connection.execute('create table tmp as {0}'.format(query))
        self.__connection.execute('create table tmp2 as select (rowid - 1) as id, * from tmp')
        self.__connection.execute('drop table {0}'.format(table_name))
        self.__connection.execute('alter table tmp2 rename to {0}'.format(table_name))
        self.__connection.execute('drop table tmp')


    '''
    Creates/rename a table

    Args:
        table_name: the new name for the table
        query: given query
    '''

    def rename_table(self, table_name: str, query: str) -> None:
        self.__connection.execute('create table tmp as {0}'.format(query))
        self.__connection.execute('drop table {0}'.format(table_name))
        self.__connection.execute('alter table tmp rename to {0}'.format(table_name))


    '''
    Cleans/compact a SQLite db
    '''

    def vacuum(self) -> None:
        #self.__connection.execute('end transaction')
        self.__connection.execute('vacuum')