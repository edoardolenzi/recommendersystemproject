#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Constants'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import os
from dotenv import load_dotenv
from os.path import join, dirname
load_dotenv(os.path.join(os.path.abspath('.'), '.env'))

def get_path(folder, file) -> str:
    return get_abs_path(os.path.join(*folder.split('.'), file))

def get_abs_path(relative_path) -> str:
    return os.path.normpath(os.path.join(os.path.abspath('.'), relative_path))

def env(key: str) -> str:
    return os.environ[key]

def set_env(key: str, value: str) -> None:
    os.environ[key] = value

# Connection strings
SQLITE_DB = 'sqlite:///csv_database.db'

# Keys
PROD_DB_KEY = 'PROD_DB'
DB_ENGINE_KEY = 'DB_ENGINE'
USER_KEY = 'USER'
PASSWORD_KEY = 'PASSWORD'
HOST_KEY = 'HOST'

# Folders
RESOURCE_FOLDER = 'rsproject.resources' 
RAW_FOLDER = RESOURCE_FOLDER + '.raw'
CHUNK_FOLDER = RESOURCE_FOLDER + '.chunk'
OUTPUT_FOLDER = RESOURCE_FOLDER + '.output'

# Files
RATINGS = 'ratings_Movies_and_TV.csv'
REVIEWS_5_CORE = 'reviews_Movies_and_TV_5.json'
REVIEWS = 'reviews_Movies_and_TV.json'
ITEMS = 'meta_Movies_and_TV.json'
SPARSE_MATRIX = 'matrix.mtx'
SPARSE_FILTERED_MATRIX = 'filtered_matrix.mtx'
SPARSE_TFIDF_MATRIX = 'tfidf_matrix.mtx'
SPARSE_USER_FEATURE_MATRIX = 'user_feature_matrix.mtx'
SPARSE_TEMP_MATRIX = 'filtered_matrix{0}.mtx'
MEAN_ADJUSTED_COSINE_MATRIX = 'mac_matrix.mtx'
DOCUMENT_CLUSTERS_CENTROIDS = 'document_centroids.json'
USER_CLUSTERS_CENTROIDS = 'user_centroids.json'
DOCUMENT_TOKEN = 'document_tokens.json'
DB_CREDENTIALS = 'db_credentials.json'

## I/O files
O_CSV_REVIEWS = 'reviews.csv'
O_CSV_ITEMS = 'items.csv'
O_CSV_CATEGORIES = 'categories.csv'
O_CSV_ITEMS_CATEGORIES = 'items_categories.csv'
O_CSV_ITEMS_SELECTED = 'items_selected.csv'
O_CSV_USERS_SELECTED = 'users_selected.csv'
O_CSV_MATRIX_SELECTED = 'matrix_selected.csv'

# Paths
RAW_RATINGS = get_path(RAW_FOLDER, RATINGS)
RAW_REVIEWS_5_CORE = get_path(RAW_FOLDER, REVIEWS_5_CORE)
RAW_REVIEWS = get_path(RAW_FOLDER, REVIEWS)
RAW_ITEMS = get_path(RAW_FOLDER, ITEMS)

CHUNK_RATINGS = get_path(CHUNK_FOLDER, RATINGS)
CHUNK_REVIEWS_5_CORE = get_path(CHUNK_FOLDER, REVIEWS_5_CORE)
CHUNK_REVIEWS = get_path(CHUNK_FOLDER, REVIEWS)
CHUNK_ITEMS = get_path(CHUNK_FOLDER, ITEMS)

## I/O file paths
O_CSV_REVIEWS_PATH = get_path(OUTPUT_FOLDER, O_CSV_REVIEWS)
O_CSV_ITEMS_CATEGORIES_PATH = get_path(OUTPUT_FOLDER, O_CSV_ITEMS_CATEGORIES)
O_CSV_CATEGORIES_PATH = get_path(OUTPUT_FOLDER, O_CSV_CATEGORIES)
O_CSV_ITEMS_PATH = get_path(OUTPUT_FOLDER, O_CSV_ITEMS)
O_CSV_MATRIX_SELECTED_PATH = get_path(OUTPUT_FOLDER, O_CSV_MATRIX_SELECTED)
O_CSV_ITEMS_SELECTED_PATH = get_path(OUTPUT_FOLDER, O_CSV_ITEMS_SELECTED)
O_CSV_USERS_SELECTED_PATH = get_path(OUTPUT_FOLDER, O_CSV_USERS_SELECTED)

DB_CREDENTIALS_PATH = get_path(RESOURCE_FOLDER, DB_CREDENTIALS)
SPARSE_MATRIX_PATH = get_path(RESOURCE_FOLDER, SPARSE_MATRIX)
SPARSE_FILTERED_MATRIX_PATH = get_path(RESOURCE_FOLDER, SPARSE_FILTERED_MATRIX)
SPARSE_TFIDF_MATRIX_PATH = get_path(RESOURCE_FOLDER, SPARSE_TFIDF_MATRIX)
SPARSE_USER_FEATURE_MATRIX_PATH = get_path(RESOURCE_FOLDER, SPARSE_USER_FEATURE_MATRIX)
SPARSE_TEMP_MATRIX_PATH = get_path(RESOURCE_FOLDER, SPARSE_TEMP_MATRIX)
MEAN_ADJUSTED_COSINE_MATRIX_PATH = get_path(RESOURCE_FOLDER, MEAN_ADJUSTED_COSINE_MATRIX)
DOCUMENT_CLUSTERS_CENTROIDS_PATH = get_path(RESOURCE_FOLDER, DOCUMENT_CLUSTERS_CENTROIDS)
USER_CLUSTERS_CENTROIDS_PATH = get_path(RESOURCE_FOLDER, USER_CLUSTERS_CENTROIDS)
DOCUMENT_TOKEN_PATH = get_path(RESOURCE_FOLDER, DOCUMENT_TOKEN)