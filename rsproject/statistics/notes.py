#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Support file for feasibility study'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import logging
import cProfile
import time
import os

from rsproject.commons.db_manager import DBManager
from rsproject.importer.importer import Importer
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.similarities.cosine_similarity import CosineSimilarity
from rsproject.similarities.mse import MSE
from rsproject.similarities.pearson_similarity import PearsonSimilarity
from rsproject.similarities.proximity_measure import ProximityMeasure
from rsproject.statistics.statistics import Statistics
from rsproject.commons.models.matrix import Matrix
import rsproject.commons.constants as const
from rsproject.content_based.text_preprocessing import TextPreprocessing
from rsproject.commons.models.token_entity import TokenEntity


class Notes():


    '''
    Support file for feasibility study
    '''


    __mse = None
    __cosine = None
    __pearson = None
    __proximity = None
    __sparse_matrix = None
    __db_manager = None
    __filtered_mtx = None
    __preproc = None
    total_ratings = None
    total_users = None
    average_ratings_per_user = None
    LOGGER = logging.getLogger(__name__)
    
    
    def __init__(self):
        self.__db_manager = DBManager()
        self.__sparse_matrix = SparseMatrix() 
        self.__filtered_mtx = self.__sparse_matrix.import_matrix(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
        self.__mse = MSE(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
        self.__cosine = CosineSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
        self.__pearson = PearsonSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
        self.__proximity = ProximityMeasure(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
        self.__preproc = TextPreprocessing()


    def get_time(self, func, *args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        return end_time - start_time        # 3


    def get_average_commons(self) -> int:
        total = 0
        current_row = 0
        for user1 in self.__filtered_mtx.matrix:
            for user2 in self.__filtered_mtx.matrix:
                current_row += 1
                self.LOGGER.debug('fatta una riga {0} su {1} \t total \t{2}'.format(current_row, self.__filtered_mtx.matrix.shape[0], total))
                total += self.get_commons(user1, user2)
        return total/(self.__filtered_mtx.matrix.shape[0] ** 2)


    def get_commons(self, user1, user2) -> int:
        i = set(user1.nonzero()[0])
        j = set(user2.nonzero()[0])
        return len(i.intersection(j))


    '''
    Profile similarities measures
    '''

    def profile_similarities(self, index1, index2) -> None:
        self.profile(self.__mse.auxiliar, index1, index2)
        self.profile(self.__cosine.auxiliar, index1, index2)
        self.profile(self.__pearson.auxiliar, index1, index2)
        self.profile(self.__proximity.auxiliar, index1, index2)


    '''
    Find the best minimum number of ratings per user (in order to filter out some data)
    '''

    def best_min_num_ratings(self):
        # Filtrando da 0 a 10 recensioni per utente
        for i in range(1, 11):

            self.LOGGER.info('\n\n ####### more than {0} reviews #######\n\n'.format(i))

            # 0. numero medio di recensioni in comune fra utenti = num ratings / num users

            total_selected_ratings = self.__db_manager.execute('select count(*) from selected_ratings{0}'.format(i)).scalar()
            total_selected_users = self.__db_manager.execute('select count(*) from selected_users{0}'.format(i)).scalar()
            average_ratings_per_user = total_selected_ratings/total_selected_users
            self.LOGGER.info('Numero medio di recensioni per utente: {0}'.format(average_ratings_per_user))


            ## 1. guardo la percentuale 

            total_filetered_users = self.__db_manager.execute('''SELECT count(*)
                                                                 FROM users 
                                                                 WHERE numberOfRatings > {0}'''.format(i)).scalar()
            self.LOGGER.info('Totale utenti con più di {0} recensioni: \t {1} \t {2}%'.format(i, total_filetered_users, total_filetered_users * 100 / self.total_users))
            

            ## 2. creo, di conseguenza le matrici filtrate

            if not os.path.isfile(const.SPARSE_TEMP_MATRIX_PATH.format(i)):
                self.__sparse_matrix.create('selected_ratings{0}'.format(i), 'selected_users{0}'.format(i), 'items')
                self.__sparse_matrix.export_matrix(Matrix(None, const.SPARSE_TEMP_MATRIX_PATH.format(i)))
            self.__filtered_mtx = self.__sparse_matrix.import_matrix(Matrix(None, const.SPARSE_TEMP_MATRIX_PATH.format(i)))


            ### 3. trovo due utenti pivot con il numero medio di recensioni in comune, 
            #      scelgo 1441346 e 98784 perche' hanno piu di 200 recensioni in comune (ampiamente sopra la media)
            
            user_pivot1 = self.__db_manager.execute('select id from selected_users where user = 1441346').scalar()
            user_pivot2 = self.__db_manager.execute('select id from selected_users where user = 98784').scalar()
            total_common_items = len(self.__cosine.get_commons(user_pivot1, user_pivot2)[0])
            self.LOGGER.info('Numero di item recensiti in comune da {0} e {1}: \t{2}'.format(user_pivot1, user_pivot2, total_common_items))
            
            ### 4. lancio le varie metriche di similarita' sui pivot per calcolare quanto ci mette a trovare le guide

            mse_time = self.get_time(self.__mse.auxiliar, user_pivot1, user_pivot2)
            cosine_time = self.get_time(self.__cosine.auxiliar, user_pivot1, user_pivot2)
            pearson_time = self.get_time(self.__pearson.auxiliar, user_pivot1, user_pivot2)
            proximity_time = self.get_time(self.__proximity.auxiliar, user_pivot1, user_pivot2)

            self.LOGGER.info('Total MSE time (one user wrt every other users) \t {0} min'.format(mse_time * total_filetered_users / 60))
            self.LOGGER.info('Total Cosine time (one user wrt every other users) \t {0} min'.format(cosine_time * total_filetered_users / 60))
            self.LOGGER.info('Total Pearson time (one user wrt every other users) \t {0} min'.format(pearson_time * total_filetered_users / 60))
            self.LOGGER.info('Total Proximity time (one user wrt every other users) \t {0} min'.format(proximity_time * total_filetered_users / 60))


    '''
    Starts to writing the notes
    '''

    def write_notes(self):

        self.total_ratings = self.__db_manager.execute('select count(*) from ratings').scalar()
        self.total_users = self.__db_manager.execute('select count(*) from users').scalar()
        self.average_ratings_per_user = self.total_ratings/self.total_users
        self.LOGGER.info('Numero medio di item in comune fra due utenti: \t {0}'.format(self.average_ratings_per_user)) 


        # Devo trovare la giusta soglia per il numero minimo di ratings

        self.best_min_num_ratings()


        ## Prove varie 

        print('MSE \t {0}'.format(self.__mse.auxiliar(1, 1)))
        print('Cosine \t {0}'.format(self.__cosine.auxiliar(1, 1)))
        print('Pearson \t {0}'.format(self.__pearson.auxiliar(1, 1)))
        print('Proximity \t {0}'.format(self.__proximity.auxiliar(1, 1)))

        print('MSE \t {0}'.format(self.__mse.auxiliar(1, 2)))
        print('Cosine \t {0}'.format(self.__cosine.auxiliar(1, 2)))
        print('Pearson \t {0}'.format(self.__pearson.auxiliar(1, 2)))
        print('Proximity \t {0}'.format(self.__proximity.auxiliar(1, 2)))

        # Osservo che ci sono item che nessuno non ha mai comprato (7K)

        total_bad_items = self.__db_manager.execute('select count(*) from items where id not in (select distinct item from ratings)').scalar()
        self.LOGGER.info('Totale item mai comperati da nessun utente \t {0}'.format(total_bad_items))
        
            
    def profile(self, function, *args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        self.LOGGER.debug(time.time())
        function(*args, **kwargs)
        self.LOGGER.debug(time.time())
        pr.disable()
        pr.print_stats(sort='time')


    def bayesian_approach(self):
        descriptions = self.__db_manager.execute('''SELECT description FROM items WHERE description is not null''')
        for desc in descriptions:
            print(desc[0])
        # Text preprocessing 
        ## stopwords removal
        temp = '''
        Mel Brooks's 1970 comedy (his second work as a film director) is based on an old Russian folktale, and was first filmed in Yugoslavia in 1927. The story concerns an old woman who reveals on her deathbed that she has hidden jewels inside one of 12 chairs that were formerly in her home but are now scattered. Ron Moody plays the poor Russian nobleman seeking them, and Dom DeLuise is his rival. After Brooks's wild and even controversial first film,The Producers,The Twelve Chairsseems relatively tame; but it is still a funny and slightly exotic work owing to its director's longtime interest in classic cinema.--Tom Keogh
        '''
        stowords_removal_time = self.get_time(self.__preproc.stopwords_removal, temp)
        self.LOGGER.info('Tempo medio per lo stopwords removal su un documento (622 caratterin): \t {0} sec'.format(stowords_removal_time))
        self.LOGGER.info('Tempo medio per lo stopwords removal da ogni documento: \t {0} min'.format((stowords_removal_time * 86335) / 60))
        
        clean = self.__preproc.stopwords_removal(temp)

        ## stemming 
        stemming_time = self.get_time(self.__preproc.stemming, clean)
        stemming = self.__preproc.stemming(clean)
        self.LOGGER.info('Tempo medio per lo stemming su un documento (65 caratteri): \t {0} sec'.format(stemming_time))
        self.LOGGER.info('Tempo medio per lo stemming da ogni documento: \t {0} min'.format((stemming_time * 86335) / 60))


    def orm_trial(self):
        session = self.__db_manager.new_session()
        entity = TokenEntity()
        entity.token = "ciaone"
        session.add(entity)
        session.commit()

# INFO:[^:]*:
# DEBUG:[^:]*:
# .,[^ )].