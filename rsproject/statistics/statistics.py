#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Computes some statistics on the datasets
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import logging
import numpy as np

from rsproject.commons import constants as const
from rsproject.commons.db_manager import DBManager
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.matrix import Matrix


class Statistics():


    ''' 
    Computes some statistics on the datasets
    '''


    __db_manager = None
    __mtx = None
    __filtered_mtx = None
    
    total_users = None
    total_filtered_users = None
    total_items = None
    total_ratings = None
    total_filtered_ratings = None
    LOGGER = logging.getLogger(__name__)
    
    
    def __init__(self):
        self.__db_manager = DBManager()
        self.__mtx = SparseMatrix().import_matrix()
        self.__filtered_mtx = SparseMatrix().import_matrix(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
        self.LOGGER.info('Matrices loadaed')


    '''
    Comuptes the mean rating per user and the average number of reviews per user 

    Args:
        matrix: the csr matrix 
    '''

    def average_rating(self, matrix: object) -> None:
        tot = 0.0
        tot_rec = 0.0
        for i in range(0, self.total_filtered_users):
            tot += (matrix[i, :].sum()/matrix[i, :].count_nonzero())
            tot_rec += matrix[i, :].count_nonzero()
        self.LOGGER.info('Media totale dei voti per utenti: {}'.format(tot/self.total_filtered_users))
        self.LOGGER.info('Media totale di recensione per utenti: {}'.format(tot_rec/self.total_filtered_users))


    '''
    Comuptes the variance on a matrix |users| x |items|

    Args:
        matrix: the csr matrix 
    '''

    def variance(self, matrix: object) -> None:
        a = 0.00
        for i in range(0, self.total_filtered_users):
            a += np.var(matrix[0, :].toarray())
        self.LOGGER.info('La varianza media: {}'.format(a/self.total_filtered_users))

    
    '''
    CLI entry point, launches the statistics computations
    '''

    def compute_statistics(self) -> None:
        self.total_users = self.__db_manager.execute('SELECT count(*) from users').scalar()
        self.total_filtered_users = self.__db_manager.execute('SELECT count(*) from selected_users').scalar()
        self.total_items = self.__db_manager.execute('SELECT count(*) FROM items').scalar()
        self.total_ratings = self.__db_manager.execute('SELECT count(*) FROM ratings').scalar()
        self.total_filtered_ratings = self.__db_manager.execute('SELECT count(*) FROM selected_ratings').scalar()

        self.LOGGER.info('Numero totale di utenti: {}'.format(self.total_users))
        self.LOGGER.info('Numero totale di utenti presi in considerazione: {}'.format(self.total_filtered_users))
        self.LOGGER.info('Numero totale di item: {}'.format(self.total_items))
        self.LOGGER.info('Numero totale di rating: {}'.format(self.total_ratings))
        self.LOGGER.info('Numero totale di rating presi in considerazione: {}'.format(self.total_filtered_ratings))
        
        self.average_rating(self.__mtx.matrix)
        self.variance(self.__mtx.matrix)        
        self.average_rating(self.__filtered_mtx.matrix)
        self.variance(self.__filtered_mtx.matrix)
