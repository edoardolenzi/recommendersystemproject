select * from items where title is null and description is null 		-- 10869 
select * from items where title is null and description is not null 	-- 89781 
select * from items where title is not null and description is null 	-- 21336 
select * from items where title is not null and description is not null -- 86335

select sum(length(description))/86335 as average_num_chars from items where title is not null and description is not null -- 622 
select length(description), * from items where title is not null and description is not null and length(description) = 622

pragma table_info(items);

select max(length(description)) from items where title is not null and description is not null --40598

select max(length(userName)) from reviews