#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''The command line interface entry point.'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import click
import os

from rsproject.importer.importer import Importer
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.matrix import Matrix
from rsproject.similarities.cosine_similarity import CosineSimilarity
from rsproject.similarities.mse import MSE
from rsproject.similarities.pearson_similarity import PearsonSimilarity
from rsproject.similarities.proximity_measure import ProximityMeasure
from rsproject.statistics.statistics import Statistics
from rsproject.statistics.notes import Notes
import rsproject.commons.constants as const
from rsproject.testing.cf_testing import CfTesting
from rsproject.testing.cb_testing import CbTesting
from rsproject.bayesian_network.migration import Migration
from rsproject.similarities.mean_adjusted_cosine_similarity import MeanAdjustedCosineSimilarity


@click.group(chain=True)
def cli():
    '''RS Project, this project aims to test the techniques learned in the course 'Recommender Systems' of the professor Carlo Tasso upon Amazon datasets.

    Credits:

        * Ups and downs: Modeling the visual evolution of fashion trends with one-class collaborative filtering R. He, J. McAuley WWW, 2016

        * Image-based recommendations on styles and substitutes J. McAuley, C. Targett, J. Shi, A. van den Hengel SIGIR, 2015
    '''
    pass


# Dataset import

@cli.command('import')
def launch_import():
    click.echo('Start importing datasets into the SQL DB')
    Importer().launch_import()


@cli.command('migration')
def launch_migration():
    click.echo('Start DB migration')
    Migration().migrate()


@cli.command('tfidf') # 11 mins
def launch_tfidf():
    click.echo('Start tfidf')
    #Migration().create_tokens_table_async()
    #Migration().create_tokens_table_thread()
    #Migration().create_tokens_table()
    Migration().create_tokens_table_thread2()
    


@cli.command('user-clustering') # 11 mins
def launch_user_clustering():
    click.echo('Start user clustering')
    Migration().user_clustering()



@cli.command('materialize')
def launch_materialize():
    click.echo('Materializes some support queries')
    Importer().materialize_projections()


# Matrix

@cli.command('create-matrix')
def launch_create_matrix():
    click.echo('Creates and exports the matrix')
    sparce_matrix = SparseMatrix()
    sparce_matrix.create('ratings', 'users', 'items')
    sparce_matrix.export_matrix()


@cli.command('create-filtered-matrix')
def launch_create_filtered_matrix():
    click.echo('Creates and exports the matrix')
    sparce_matrix = SparseMatrix()
    sparce_matrix.create('selected_ratings', 'selected_users', 'items')
    sparce_matrix.export_matrix(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))


@cli.command('import-matrix')
def launch_import_matrix():
    click.echo('Creates and exports the matrix')
    SparseMatrix().import_matrix(Matrix(None, const.SPARSE_TEMP_MATRIX_PATH.format('5')))


# Statistics

@cli.command('statistics')
def launch_statistics():
    click.echo('Start computing statistics')
    Statistics().compute_statistics()


# Similarities 

@cli.command('cosine-similarity')
def launch_cosine_sililarity():
    click.echo('Start computing cosine similarity')
    sim = CosineSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))


@cli.command('mse')
def launch_mse():
    click.echo('Start computing mse')
    sim = MSE(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))


@cli.command('pearson-similarity')
def launch_pearson_similarity():
    click.echo('Start computing brutal pearson similarity')
    sim = PearsonSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))


@cli.command('proximity-measure')
def launch_proximity_measure():
    click.echo('Start computing proximity measure')
    sim = ProximityMeasure(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))


@cli.command('mean-adjusted')
def launch_mean_adjusted():
    click.echo('Start computing mean adjusted cosine similarity')
    sim = MeanAdjustedCosineSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))


# Predictions

@cli.command('cf-prediction')
def launch_cf_prediction():
    click.echo('Start computing CF prediction')
    click.echo('Start computing cosine similarity')
    sim = CosineSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))
    click.echo('Start computing mse')
    sim = MSE(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))
    click.echo('Start computing brutal pearson similarity')
    sim = PearsonSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))
    #click.echo('Start computing proximity measure')
    #sim = ProximityMeasure(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    #sim.execute(int(const.env('P_USER1_INDEX')))
    click.echo('Start computing mean adjusted cosine similarity')
    sim = MeanAdjustedCosineSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    sim.execute(int(const.env('P_USER1_INDEX')))


# Testing

@cli.command('cf-test')
@click.argument('metric')
def launch_cf_test(metric: str):
    click.echo('Start computing CF test')
    test = CfTesting(int(const.env('P_USER1_INDEX')), Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    test.test(metric) 


@cli.command('cf-evaluation-ub')
@click.argument('metric')
def launch_cf_evaluation_ub(metric: str):
    click.echo('Start computing CF test')
    test = CfTesting(int(const.env('P_USER1_INDEX')), Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    test.evaluation_user_based(metric) 


@cli.command('cf-evaluation-ib')
@click.argument('metric')
def launch_cf_evaluation_ib(metric: str):
    click.echo('Start computing CF test')
    test = CfTesting(int(const.env('P_USER1_INDEX')), Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    test.evaluation_item_based() 


@cli.command('cb-test')
def launch_cb_test():
    click.echo('Start computing CB test')
    CbTesting(1).confronto_tre_tecniche()


# Notes

@cli.command('notes')
def launch_notes():
    click.echo('Start computing notes')
    Notes().write_notes()


@cli.command('notes1')
def launch_notes1():
    click.echo('Start computing notes')
    Notes().bayesian_approach()

# Current environment state

@cli.command('environment')
def describe_environment():
    # for key in os.environ:
    #     click.echo(key + '\t ' + os.environ[key])
    click.echo('DB_CONTEXT \t\t ' + os.environ['DB_CONTEXT'])
    click.echo('DATA \t\t\t ' + os.environ['DATA'])
    click.echo('IMPORT_REVIEWS \t\t ' + os.environ['IMPORT_REVIEWS'])
    click.echo('REMOVE_OUTPUT \t\t ' + os.environ['REMOVE_OUTPUT'])
    click.echo('MTX_DIM \t\t ' + os.environ['MTX_DIM'])
    click.echo('LOG_LEVEL \t\t ' + os.environ['LOG_LEVEL'])
    click.echo('---------------------------------')
    click.echo('P_USER1_INDEX \t\t ' + os.environ['P_USER1_INDEX'])
    click.echo('P_USER2_INDEX \t\t ' + os.environ['P_USER2_INDEX'])
    click.echo('RATING_TRASHOLD \t ' + os.environ['RATING_TRASHOLD'])
    click.echo('N_GUIDES \t\t ' + os.environ['N_GUIDES'])
    click.echo('TO_PREDICT_ITEM \t ' + os.environ['TO_PREDICT_ITEM'])
    click.echo('TQDM \t ' + os.environ['TQDM'])
    click.echo('N_TOKENS \t ' + os.environ['N_TOKENS'])
    click.echo('N_DOC_CLUSTERS \t ' + os.environ['N_DOC_CLUSTERS'])

# launch with python -m rsproject importer into pipenv
# > python3 -m pipenv shell
# > python3 -m rsproject
