#!/usr/bin/env python3
# coding: utf-8

'''DB Migration for clustering import'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import logging
import asyncio
import _thread
from multiprocessing.dummy import Pool as ThreadPool 
import numpy as np
import itertools
from sqlalchemy import func
from scipy.sparse import csr_matrix
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import AffinityPropagation
from sklearn.cluster import MeanShift
from sklearn.cluster import MiniBatchKMeans
from sklearn.cluster import KMeans

from rsproject.commons.db_manager import DBManager
from rsproject.commons import constants as const
from rsproject.commons.models.token_entity import TokenEntity
from rsproject.commons.models.document_token_entity import DocumentTokenEntity
from rsproject.commons.models.document_entity import DocumentEntity
from rsproject.commons.models.matrix import Matrix
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.cluster_entity import DocumentClusterEntity
from rsproject.commons.models.cluster_entity import UserClusterEntity
from rsproject.commons.models.rating_entity import RatingEntity
from rsproject.commons.models.item_entity import ItemEntity
from rsproject.commons.models.user_entity import UserEntity
from rsproject.content_based.text_preprocessing import TextPreprocessing
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.decorators.async import force_async
from rsproject.importer.importer import Importer


@for_all_methods(timer)
class Migration():


    '''DB Migration, creates new support tables for the probabilistic bayesian network approach'''
    

    LOGGER = logging.getLogger(__name__)
    __db_engine = None
    __db_manager = None
    __preprocessing = None
    __session = None
    __importer = None
    __doc_tokens = []


    def __init__(self):
        self.__db_manager = DBManager()
        self.__preprocessing = TextPreprocessing()
        self.__session = self.__db_manager.new_session()
        self.__importer = Importer()


    def migrate(self) -> None:
        self.LOGGER.info('Migration started')
        self.create_tokens_table()
        self.LOGGER.info('Token imported')
        self.tfidf()
        self.LOGGER.info('Tf*idf importer')
        self.user_clustering()
        self.LOGGER.info('User imported')


    @force_async
    def swr_wrapper(self, id, description):
        self.__doc_tokens.append(self.__preprocessing.stemming(self.__preprocessing.stopwords_removal(description)))
    

    def swr_wrapper2(self, id, description):
        self.__doc_tokens.append(self.__preprocessing.stemming(self.__preprocessing.stopwords_removal(description)))

    
    def create_tokens_table(self) -> None:
        descriptions = self.__db_manager.execute('''SELECT id, description FROM items WHERE description is not null LIMIT 100000''')

        for id, description in descriptions:
            clean = self.__preprocessing.stopwords_removal(description)
            self.__doc_tokens.append((id, self.__preprocessing.stemming(clean)))


    def create_tokens_table_thread(self) -> None:
        descriptions = self.__db_manager.execute('''SELECT id, description FROM items WHERE description is not null LIMIT 100000''')
        for id, description in descriptions:
            _thread.start_new_thread( self.swr_wrapper2, (id, description, ) )
            

    def create_tokens_table_async(self) -> None:
        descriptions = self.__db_manager.execute('''SELECT id, description FROM items WHERE description is not null LIMIT 100000''')

        loop = asyncio.get_event_loop()
        tasks = []
        for id, description in descriptions:
            tasks.append(asyncio.ensure_future(self.swr_wrapper(id, description)))

        loop.run_until_complete(asyncio.wait(tasks))  
        loop.close()


    def create_tokens_table_thread2(self) -> None:
        descriptions = self.__db_manager.execute('''SELECT id, description FROM items WHERE description is not null LIMIT 100000''')

        pool = ThreadPool(3) 
        for id, description in descriptions:
            pool.map(self.swr_wrapper, zip(itertools.repeat(id), itertools.repeat(description)))


    def create_tokens_table(self) -> None:
        descriptions = self.__db_manager.execute('''SELECT id, description FROM items WHERE description is not null''')

        for id, description in descriptions:
            clean = self.__preprocessing.stopwords_removal(description)
            self.__doc_tokens.append((id, self.__preprocessing.stemming(clean)))

        self.__importer.serialize(self.__doc_tokens, const.DOCUMENT_TOKEN_PATH)
        self.__doc_tokens = self.__importer.deserialize(const.DOCUMENT_TOKEN_PATH)

        tokens = []

        # TODO optimize this cycle
        for (id, tokens_list) in self.__doc_tokens:
            tokens = tokens + tokens_list
        tokens = list(set(tokens))

        self.LOGGER.info('Tokens extracted: \t {0}'.format(len(tokens)))

        #query = self.__session.query(TokenEntity.token).all()
        #tokens = list(zip(*query))[0]

        token_dict = {}
        for index, token in enumerate(tokens):
            token_dict[token] = index

        self.LOGGER.info('Tokens retrieved')

        self.__db_manager.execute('CREATE TABLE tokens (id int, token varchar(255))')
        token_dict = {}
        for index, token in enumerate(tokens):
            entity = TokenEntity()
            entity.token = token            
            entity.id = index
            self.__session.add(entity)
            token_dict[token] = index
        self.__session.commit()

        self.LOGGER.info('Tokens table populated')

        self.__db_manager.execute('CREATE TABLE documents_tokens (id int, token int, document int)')
        self.__db_manager.execute('CREATE TABLE documents (id int, item int, description varchar(76946))')
        c = 0

        for index, (id, tokens_list) in enumerate(self.__doc_tokens):
            document = DocumentEntity()
            document.description = ''

            for token in tokens_list:
                c += 1
                entity = DocumentTokenEntity()
                entity.id = index            
                entity.token = token_dict[token]
                entity.document = id
                document.id = id 
                document.item = id 
                document.description = document.description + ' ' + token
                self.__session.add(document)
                self.__session.add(entity)

                # In order to prevento memory errors
                if c % 100000 == 0:
                    self.__session.commit()

            document = None
        self.__session.commit()
        self.LOGGER.info('DocumentTokens populated')
        

    def tfidf(self) -> None: 
        query = self.__session.query(DocumentEntity.description).all()
        descriptions = list(zip(*query))[0]
        tfidf = TextPreprocessing().tfidf_matrix(descriptions)
        self.LOGGER.info('Tf*idf computed')

        column_count = [[i, 0] for i, x in enumerate(range(tfidf.shape[1]))]

        for i in tfidf:
            for index in i.indices:
                column_count[index][1] += 1

        i = 0
        while len(column_count) > int(const.env('N_TOKENS')):
            # TODO save index mapping
            column_count = [x for x in column_count if x[1] > i]
            i += 1
        
        filtered_tfidf = tfidf[:, [x[0] for x in column_count]]
        self.LOGGER.info('Tf*idf filtered')
        SparseMatrix().export_matrix(Matrix(filtered_tfidf, const.SPARSE_TFIDF_MATRIX_PATH))

        clustering = MiniBatchKMeans(n_clusters=int(const.env('N_DOC_CLUSTERS')), 
                                     init_size=3*int(const.env('N_DOC_CLUSTERS')), 
                                     random_state=0, 
                                     batch_size=6, 
                                     max_iter=10).fit(filtered_tfidf.toarray())

        #clustering = KMeans(n_clusters=100, random_state=0).fit(filtered_tfidf.toarray())
        #clustering = AffinityPropagation().fit(filtered_tfidf.toarray())
        #clustering = AgglomerativeClustering().fit(filtered_tfidf.toarray()) # only 
        #clustering = MeanShift().fit(filtered_tfidf.toarray()) # slow on huge numbers

        centroids = clustering.cluster_centers_ 
        self.__importer.serialize(centroids.tolist(), const.DOCUMENT_CLUSTERS_CENTROIDS_PATH)
        self.__db_manager.execute('CREATE TABLE document_clusters (id int, document int, cluster int)')

        for i in range(clustering.labels_.size):
            entity = DocumentClusterEntity()
            entity.id = i 
            entity.document = i 
            entity.cluster = int(clustering.labels_[i])         
            self.__session.add(entity)

        self.__session.commit()   
        self.LOGGER.info('document clusters saved on DB')


    def user_clustering(self) -> None:

        total_users = self.__session.query(func.count(UserEntity.id)).scalar()
        total_clusters = int(const.env('N_DOC_CLUSTERS')) 

        query = self.__session.query(RatingEntity.user, DocumentClusterEntity.cluster, func.count(DocumentClusterEntity.cluster), ItemEntity, DocumentEntity).\
                                       filter(RatingEntity.item == ItemEntity.id).\
                                       filter(DocumentEntity.item == ItemEntity.id).\
                                       filter(DocumentClusterEntity.document == DocumentEntity.id).\
                                       group_by(DocumentClusterEntity.cluster, RatingEntity.user).\
                                       all()

        document_clusters = list(zip(*query))

        row = document_clusters[0]
        col = document_clusters[1]
        data = document_clusters[2]

        user_features = csr_matrix((data,(row,col)), shape=(total_users, total_clusters), dtype=np.int)
        SparseMatrix().export_matrix(Matrix(user_features, const.SPARSE_TFIDF_MATRIX_PATH))

        clustering = MiniBatchKMeans(n_clusters=int(const.env('N_USER_CLUSTERS')), 
                                     init_size=3*int(const.env('N_USER_CLUSTERS')), 
                                     random_state=0, 
                                     batch_size=6, 
                                     max_iter=10).fit(user_features.toarray())

        centroids = clustering.cluster_centers_   
        self.__importer.serialize(centroids.tolist(), const.USER_CLUSTERS_CENTROIDS_PATH)

        self.__db_manager.execute('CREATE TABLE user_clusters (id int, user int, cluster int)')
        for i in range(clustering.labels_.size):
            entity = UserClusterEntity()
            entity.id = i 
            entity.user = i 
            entity.cluster = int(clustering.labels_[i])   
            self.__session.add(entity)

        self.__session.commit()   

        self.LOGGER.info('user clusters saved on DB')