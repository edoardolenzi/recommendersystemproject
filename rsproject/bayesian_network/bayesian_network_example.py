#!/usr/bin/env python3
# coding: utf-8

'''Bayesian network'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import logging
import ast
import os
import numpy as np
from pomegranate import *
from sqlalchemy import func

from rsproject.commons.db_manager import DBManager
from rsproject.content_based.text_preprocessing import TextPreprocessing
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix
from rsproject.commons.models.cluster_entity import DocumentClusterEntity 
from rsproject.commons.models.cluster_entity import UserClusterEntity
from rsproject.commons.models.rating_entity import RatingEntity


@for_all_methods(timer)
class BNExample():


    '''Bayesian network example implementation'''
    

    LOGGER = logging.getLogger(__name__)
    __db_engine = None
    __db_manager = None
    __preprocessing = None
    __session = None
    __doc_tokens = []


    def __init__(self):
        # creates a DB connection
        self.__db_manager = DBManager()
        self.__preprocessing = TextPreprocessing()
        self.__session = self.__db_manager.new_session()


    '''
    We have a layer of n features F1, .., Fn with some associated intervals as 
    domain values.
        * The intervals are infered from the vectorial representations of the centroids 
            of the user clusters 
    We have another layer of m user clusters with boolean values T/F as domain 
    (the user can or couldn't be in the target cluster) 
        * Let a node with i > 0 fathers, the CPT of the node must contains the 
            conditional probability of every possible combintions of the values 
            in the domains of the fathers  
    Finally we have a layer of document cluster D1,..,Dp that are the output of the 
    algorithm. 
    Once that we have selected the best document cluster for the user we can apply 
    a CB algorithm in order to predict some products of that cluster.
    '''

    def get_keys(self, discrete_distribution: DiscreteDistribution) -> list:
        return list(discrete_distribution.summaries[0].keys())

    def get_val(self, discrete_distribution: DiscreteDistribution, key: str) -> float:
        return discrete_distribution.parameters[0][key]

    def example(self) -> None:


        # use dictionaries for the nodes in order to make a fast lookup 
        # from the name of the node (a string)
        
        features = {}
        users = {}
        documents = {}


        # centroids of the user clusters
        
        user_centralities = np.matrix([ 
            [1, 3, 7, 0, 0],
            [0, 0, 4, 0, 0],
            [0, 0, 10, 3, 2]])

        temp = user_centralities.copy()
        temp = temp.T
        temp.sort()


        # Create some intervals from the centroids in order change the domain 
        # from contiuous to discrete

        intervals = []          # Each feature is a random variable with some intervals 
                                # as domain (with the correspondent probability associated)
        feature_buckets = {}    # Associate every user cluster's centroid to an interval
        

        # Query for retrieve documents and users clusters joined with users 
        # (in order to populate the feature layer of the network)
        # The user for a given document cluster has tot rating
        # [count(user), doc_cluster, user, user_cluster] 

        general_query = self.__session.query(func.count(RatingEntity.user), 
                                             DocumentClusterEntity.cluster, 
                                             RatingEntity.user, 
                                             UserClusterEntity.cluster).\
            filter(UserClusterEntity.user == RatingEntity.user).\
            filter(DocumentClusterEntity.document == RatingEntity.item).\
            group_by(RatingEntity.user).\
            all()


        # Computes the intervals (based on the centroids of the user clusters)

        for index, row in enumerate(temp):

            # filter out the general query to obtain the users related to a document cluster            
            
            query = [x for x in general_query if x[1] == index]

            maximum = max(list(zip(*query))[0])
            total = sum(list(zip(*query))[0])

            buckets = set(row.flat)
            buckets.add(0)
            buckets.add(maximum)
            buckets = list(buckets)
            buckets.sort()

            last = buckets[0]
            tmp = []
            for j in range(len(buckets) - 1):
                middle = (buckets[j] + buckets[j + 1])/2.
                tmp.append([last, middle])
                last = middle
            tmp.append([last, buckets[len(buckets) - 1]])

            for couple in tmp:
                couple.append(sum([x[0] for x in query if x[0] >= couple[0] and x[0] < couple[1]]) / total)
            
            intervals.append(tmp)


        # Connects the centroids to the correspondent interval

        for i, row in enumerate(user_centralities):
            temp = []
            for val in list(row.flat):
                temp.append([i for i, x in enumerate(intervals[i]) if x[0] <= val < x[1]][0])
            feature_buckets['U{0}'.format(i + 1)] = temp


        # Creates the feature nodes (F0, F1, ...)

        for i, feature in enumerate(intervals):
            features['F{0}'.format(i + 1)] = DiscreteDistribution(dict([[str(index), tpl[2]] for index, tpl in enumerate(feature)]))

        #features['F{0}'.format(i + 1)] = DiscreteDistribution({'T': 1./2, 'F': 1./2})
        #features['F1'] = DiscreteDistribution({'T': 1./2, 'F': 1./2})
        #features['F2'] = DiscreteDistribution({'A': 1./3, 'B': 1./3, 'C': 1./3})
        #features['F3'] = DiscreteDistribution({'A': 1./4, 'B': 1./4, 'C': 1./4, 'D': 1./4})
        #features['F4'] = DiscreteDistribution({'A': 1./5, 'B': 1./5, 'C': 1./5, 'D': 1./5, 'D': 1./5})
        #features['F5'] = DiscreteDistribution({'A': 1./6, 'B': 1./6, 'C': 1./6, 'D': 1./6, 'D': 1./6, 'D': 1./6})

        # TODO remember to crete the UC nodes

        features['U1'] = DiscreteDistribution({'T': 1./2, 'F': 1./2})
        features['U2'] = DiscreteDistribution({'T': 1./2, 'F': 1./2})
        features['U3'] = DiscreteDistribution({'T': 1./2, 'F': 1./2})


        # TODO todo find the parents

        parents = {
            'U1': ['F1', 'F2', 'F3', 'U1'],
            'U2': ['F3', 'U2'],
            'U3': ['F3', 'F4', 'F5', 'U3'],
            #'D1': ['U1'],
            #'D2': ['U1'],
            #'D3': ['U1', 'U2', 'U3'],
            #'D4': ['U3'],
            #'D5': ['U3']
        }


        # Creates the CPT for the UCs 

        for node in parents.keys():
            total_values = 1

            # For each parent
            for parent in parents[node]:
                total_values *= len(self.get_keys(features[parent]))

            #mytype = ''.join('S10, ' for i in range(len(parents[node]))) + 'f4'   
            mtx = np.empty([total_values, len(parents[node]) + 1], dtype='U10')

            for col in range(mtx.shape[1] - 1):
                parent = parents[node][col]
                total_values = total_values / len(self.get_keys(features[parent]))


                for row in range(mtx.shape[0]):
                    values = self.get_keys(features[parent])
                    cur_val = values[int((row/total_values) % len(values))]
                    mtx[row][col] = '{0}'.format(cur_val)
                

            # We have the matrix with the combinations, now associates the conditional probability
            ## P(U1|F1,F2,F3) = P(U1) * P(F1,F2,F3|U1) / P(F1,F2,F3)

            for index, row in enumerate(mtx):
                row = row.flat
                PU1 = self.get_val(features[node], row[len(row) - 2])
                PF13 = 1
                for i, parent in enumerate(parents[node][:-1]):
                    PF13 *= self.get_val(features[parent], row[i])
                query = [x for x in general_query if 'U{0}'.format(x[3]) == node]
                total = len(query)
#                PF13U1 = len([x for x in query if 'F{0}'.format(x[1]) >= intervals[index] couple[0] and x[0] < couple[1]]) / total
#                pf = self.get_val(features[node], row[len(row) - 1])
#                prob = (pu * pfall ) / pf
#        
#                for parent2 in parents[node]:
#                    
#                    for value in self.get_keys(features[parent2]):
#                        print('[{0},{1}] {2}'. format(parent1, parent2, value))
#                        counter += 1
#            print('counter {0}'.format(counter))
#                num_values = len([x for x in get_keys(feature[parent])])
#
#                get_keys(feature[parent])
#                for i in parents[node].re:
#                cpt_array = 


            CPT = ConditionalProbabilityTable([cpt_array], [features[x] for x in parents[node]])