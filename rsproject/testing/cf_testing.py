#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Collaborative filtering testing tool'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import time 

from rsproject.predictions.prediction import Prediction
from rsproject.similarities.mse import MSE
from rsproject.similarities.proximity_measure import ProximityMeasure
from rsproject.similarities.pearson_similarity import PearsonSimilarity
from rsproject.similarities.cosine_similarity import CosineSimilarity
from rsproject.similarities.mean_adjusted_cosine_similarity import MeanAdjustedCosineSimilarity
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.matrix import Matrix
import random

class CfTesting():


    __user = None
    __mtx = None
    __matrix = None 


    def __init__(self, user: int, matrix: Matrix):
        self.__user = user
        self.__matrix = matrix
        self.__mtx = SparseMatrix().import_matrix(matrix)


    '''
        Factory used to launch the right similarity metric by name

        Args:
            metric: metric name
            matrix: Matrix = {matrix: csr matrix, matrix_path: string identifier for the matrix}

        Returns:
            An array with the result of the metric execution
    '''

    def __check_metric(self, metric: str, matrix: Matrix) -> []:
        metrica = metric.lower()
        if metrica == 'similarity':
            temp = ProximityMeasure(matrix)
        if metrica == 'mse':
            temp = MSE(matrix)
        if metrica == 'pearson':
            temp = PearsonSimilarity(matrix)
        if metrica == 'cosine':
            temp = CosineSimilarity(matrix)
        return temp.execute(self.__user)


    '''
        Fanction for the testing of the choosed metric

        Args:
            metric: metric name
    '''

    def test(self, metric: str) -> None:
        # prendo gli elementi diversi da 0 dell'utente pivot
        non_zero_elements = self.__mtx.matrix[self.__user, :].nonzero()
        # conto elementi diversi da zero
        lenght_non_zero = len(non_zero_elements[1])
        # calcolo il 20 degli elementi diversi da zero sui quali farò il testing
        percentage = int(lenght_non_zero * 0.2)

        # variabili per le medie finali
        final_sum_with_zero = 0
        final_sum_without_zero = 0

        for kk in range(5):
            #for index in range(0, lenght_non_zero - percentage):
            # index = 0 TODO (?)
            # creo la nuova matrice su cui fare il training
            new_matrix = Matrix(self.__mtx.matrix[:, :], time.time())
            print('Faccio il testing da {} a {}'.format(0, percentage))
            dele = random.sample(list(non_zero_elements[1]), len(non_zero_elements[1]))
            # azzero i valore che fanno parte del 20%
            for i in dele[0:percentage]:
                new_matrix.matrix[self.__user, i] = 0

            # faccio il training dell'80%
            pred = Prediction(new_matrix, self.__user)
            measure = self.__check_metric(metric, new_matrix)
            # conto la media della variazione tra i voti previsti e quelli realmente dati
            # somma senza contare gli zeri (item che si è predetto non avrebbe contanto e invece l'ha fatto)
            non_zero_sum = 0.00
            # somma contando gli zeri
            with_zero_sum = 0.00
            # counter per le due somme 
            zero_counter = 0
            non_zero_counter = 0

            for i in dele[0:percentage]:
                # predico il voto
                predicted = pred.predict_for_training(measure, i)

                if predicted != 0.00:
                    non_zero_sum += predicted - self.__mtx.matrix[self.__user, i]
                    non_zero_counter += 1
                else:
                    zero_counter += 1
                with_zero_sum += predicted - self.__mtx.matrix[self.__user, i]

            # output
            with_zero_mean = with_zero_sum / percentage
            non_zero_mean = non_zero_sum / non_zero_counter

            final_sum_with_zero += with_zero_mean
            final_sum_without_zero += non_zero_mean

            print('Mean whit zero: {}'.format(with_zero_mean))
            print('Mean whitout zero: {}'.format(non_zero_mean))
            print('Total ratings: {}\nNon zero ratings: {}'.format(percentage, non_zero_counter))
        
        print("Final mean with zero {}".format(final_sum_with_zero / 5))
        print("Final mean whitout zero {}".format(final_sum_without_zero / 5))


    def precision(self, dictionary: dict) -> float:
        return dictionary['TP'] / (dictionary['TP'] + dictionary['FP'])


    def recall(self, dictionary: dict) -> float:
        return dictionary['TP'] / (dictionary['TP'] + dictionary['FN'])


    def f1(self, dictionary: dict) -> float:
        recall = self.recall(dictionary)
        precision = self.precision(dictionary)
        print('TP: {0}, TN: {1}, FP: {2}, FN: {3}'.format(dictionary['TP'], dictionary['TN'], dictionary['FP'], dictionary['FN']))
        print('recall = {0}'.format(recall))
        print('precision = {0}'.format(precision))
        f1 = (2 * precision * recall)/(precision + recall)
        print('f1 score = {0}'.format(f1))
        return f1


    '''
        Computed precision, recall and F1 score

        Args:
            metric: metric name
    '''

    def evaluation_user_based(self, metric: str) -> None:
         # prendo gli elementi diversi da 0 dell'utente pivot
        non_zero_elements = self.__mtx.matrix[self.__user, :].nonzero()
        # conto elementi diversi da zero
        lenght_non_zero = len(non_zero_elements[1])
        # calcolo il 20 degli elementi diversi da zero sui quali farò il testing
        percentage = int(lenght_non_zero * 0.2)

        for kk in range(5):
            #for index in range(0, lenght_non_zero - percentage):
            # index = 0 TODO (?)
            # creo la nuova matrice su cui fare il training
            new_matrix = Matrix(self.__mtx.matrix[:, :], time.time())
            print('Faccio il testing da {} a {}'.format(0, percentage))
            dele = random.sample(list(non_zero_elements[1]), len(non_zero_elements[1]))
            # azzero i valore che fanno parte del 20%
            for i in dele[0:percentage]:
                new_matrix.matrix[self.__user, i] = 0

            # faccio il training dell'80%
            pred = Prediction(new_matrix, self.__user)
            measure = self.__check_metric(metric, new_matrix)
            # conto la media della variazione tra i voti previsti e quelli realmente dati
            # somma senza contare gli zeri (item che si è predetto non avrebbe contanto e invece l'ha fatto)

            confusion_matrix = {'TP': 0, 'TN': 0, 'FP': 0, 'FN': 0}

            for i in dele[0:percentage]:
                # predico il voto
                
                real_value = self.__mtx.matrix[self.__user, i]
                predicted_value = pred.predict_for_training(measure, i)

                true = 3 <= real_value <= 5
                positive = 3 <= predicted_value <= 5

                if true and positive:
                    confusion_matrix['TP'] += 1
                elif not true and positive:
                    confusion_matrix['FP'] += 1
                elif not true and not positive:
                    confusion_matrix['FN'] += 1
                elif true and not positive:
                    confusion_matrix['TN'] += 1

            self.f1(confusion_matrix)


    def evaluation_item_based(self) -> None:
         # prendo gli elementi diversi da 0 dell'utente pivot
        non_zero_elements = self.__mtx.matrix[self.__user, :].nonzero()
        # conto elementi diversi da zero
        lenght_non_zero = len(non_zero_elements[1])
        # calcolo il 20 degli elementi diversi da zero sui quali farò il testing
        percentage = int(lenght_non_zero * 0.2)


        for kk in range(1):
            #for index in range(0, lenght_non_zero - percentage):
            # index = 0 TODO (?)
            # creo la nuova matrice su cui fare il training
            new_matrix = Matrix(self.__mtx.matrix[:, :], time.time())
            print('Faccio il testing da {} a {}'.format(0, percentage))
            dele = random.sample(list(non_zero_elements[1]), len(non_zero_elements[1]))
            # azzero i valore che fanno parte del 20%
            for i in dele[0:percentage]:
                new_matrix.matrix[self.__user, i] = 0

            # faccio il training dell'80%
            pred = Prediction(new_matrix, self.__user)
            # conto la media della variazione tra i voti previsti e quelli realmente dati
            # somma senza contare gli zeri (item che si è predetto non avrebbe contanto e invece l'ha fatto)

            confusion_matrix = {'TP': 0, 'TN': 0, 'FP': 0, 'FN': 0}

            for i in dele[0:percentage]:
                # predico il voto
                
                real_value = int(self.__mtx.matrix[self.__user, i])
                predicted_value = pred.predict_for_training_item_based(self.__user, int(i))

                true = 3 <= real_value <= 5
                positive = 3 <= predicted_value <= 5

                if true and positive:
                    confusion_matrix['TP'] += 1
                elif not true and positive:
                    confusion_matrix['FP'] += 1
                elif not true and not positive:
                    confusion_matrix['FN'] += 1
                elif true and not positive:
                    confusion_matrix['TN'] += 1

            self.f1(confusion_matrix)