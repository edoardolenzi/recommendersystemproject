import click
import os
import re

# commons
import rsproject.commons.constants as const
import rsproject.commons.localizations as loc
from rsproject.commons.db_manager import DBManager
from rsproject.commons.sparse_matrix import SparseMatrix

# content based
from rsproject.content_based.text_preprocessing import TextPreprocessing

# decorators
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.singleton import singleton
from rsproject.decorators.timer import timer

# importer
from rsproject.importer.importer import Importer

# predictions
from rsproject.predictions.prediction import Prediction

# similarities
from rsproject.similarities.base_similarity import BaseSimilarity
from rsproject.similarities.cosine_similarity import CosineSimilarity
from rsproject.similarities.mse import MSE
from rsproject.similarities.pearson_similarity import PearsonSimilarity
from rsproject.similarities.proximity_measure import ProximityMeasure

# statistics
from rsproject.statistics.statistics import Statistics
from rsproject.statistics.notes import Notes

# testing
from rsproject.testing.cf_testing import CfTesting
from rsproject.testing.cb_testing import CbTesting

db_manager = DBManager()
sparse_matrix = SparseMatrix()
txt_preproc = TextPreprocessing()
importer = Importer()

prediction = Prediction(const.SPARSE_FILTERED_MATRIX_PATH, 0)

cosine = CosineSimilarity(const.SPARSE_FILTERED_MATRIX_PATH)
proximity = ProximityMeasure(const.SPARSE_FILTERED_MATRIX_PATH)
pearson = PearsonSimilarity(const.SPARSE_FILTERED_MATRIX_PATH)
mse = MSE(const.SPARSE_FILTERED_MATRIX_PATH)

statistics = Statistics()
notes = Notes()

cf_testing = CfTesting
cb_testing = CbTesting