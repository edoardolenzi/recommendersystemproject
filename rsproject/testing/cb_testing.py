#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Content based testing'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


from nltk.corpus import stopwords 
from tqdm import tqdm
import logging

from rsproject.content_based.text_preprocessing import TextPreprocessing
from rsproject.commons.db_manager import DBManager
import rsproject.commons.constants as const


class CbTesting():


    __item = None
    __db_manager = None
    __preprocesser = None
    __categories_dict = None
    __items_dict = None
    __docs = []
    LOGGER = logging.getLogger(__name__)


    def __init__(self, item):
        self.__item = item
        self.__db_manager = DBManager()
        self.__preprocesser = TextPreprocessing()
        self.__categories_dict = self.__create_categories_dict()
        self.__items_dict = self.__create_items_dict()


    '''
    Function for the creation of a dictionary with categories

    Returns:
        Dictionary {item_id, [categories]}
    '''

    def __create_categories_dict(self) -> dict:
        categories = self.__db_manager.execute('''
            SELECT i.id, c.id
            FROM categories c, items_categories ic, items i 
            WHERE ic.item = i.id and ic.category = c.id
            ORDER BY i.id''')
        categories_dict = {}
        for (cod, cat) in categories:
            if cod not in categories_dict:
                categories_dict[cod] = []
                categories_dict[cod].append(cat)
            else:
                categories_dict[cod].append(cat)
        return categories_dict


    '''
    Function for the creation of a dictionary with items with the 
    tokenization of the description (apply stopword removal)

    Returns:
        Dictionary {item_id, [tokens]}
    '''

    def __create_items_dict(self) -> dict:
        all_items = self.__db_manager.execute('''
            SELECT item, description 
            FROM selected_items''')
        items_dict = {}
        for (cod, desc) in all_items:
            #items_dict[str(cod)] = self.__preprocesser.stemming(self.__preprocesser.stopwords_removal(desc))
            items_dict[cod] = self.__preprocesser.stopwords_removal(desc)
            self.__docs.append(items_dict[cod])
        return items_dict


    '''
    Function for testing of jaccard, dice and cosine
    '''

    def confronto_tre_tecniche(self) -> None:
        results_list_cosine = []
        results_list_jaccard = []
        results_list_dice = []
        item_desc = self.__items_dict[ITEM]
        item_cat = self.__categories_dict[ITEM]
        for (key, value) in tqdm(self.__categories_dict.items(), total=len(self.__categories_dict.items()), disable=bool(int(const.env('TQDM')))):
            
            value_categories_cosine = self.__preprocesser.cosine(item_cat, value)
            value_categories_jaccard = self.__preprocesser.jaccard(item_cat, value)
            value_categories_dice = self.__preprocesser.dice(item_cat, value)
            
            if key in self.__items_dict:
                temp = self.__items_dict[key]
                value_description_cosine = self.__preprocesser.cosine(item_desc, temp)
                value_description_jaccard = self.__preprocesser.jaccard(item_desc, temp)
                value_description_dice = self.__preprocesser.dice(item_desc, temp)
            
            results_list_cosine.append((key, value_categories_cosine * value_description_cosine))
            results_list_jaccard.append((key, value_categories_jaccard * value_description_jaccard))
            results_list_dice.append((key, value_categories_dice * value_description_dice))

        results_list_cosine.sort(key=lambda tup: tup[1], reverse=True)
        results_list_jaccard.sort(key=lambda tup: tup[1], reverse=True)
        results_list_dice.sort(key=lambda tup: tup[1], reverse=True)
        self.LOGGER.info('Cosine: {}'.format(results_list_cosine[1:4]))
        self.LOGGER.info('Jaccard: {}'.format(results_list_jaccard[1:4]))
        self.LOGGER.info('Dice: {}'.format(results_list_dice[1:4]))


    '''
    Function for testing of tf*idf
    '''

    def test_tfidf(self):
        self.LOGGER.info(self.__preprocesser.tfidf_cosine(self.__item, self.__docs))


    '''
    Function for testing of lsi (with both methods)
    '''

    def test_lsi(self):
        self.LOGGER.info(self.__preprocesser.lsi(self.__docs, self.__item))
        self.LOGGER.info(self.__preprocesser.lsi(self.__docs, self.__item, False))
