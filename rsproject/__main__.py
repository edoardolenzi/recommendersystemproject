import os
import logging

from rsproject import cli
from rsproject.commons import constants as const


# set logging level
log_level = logging.INFO

if const.env('LOG_LEVEL') == 'CRITICAL':
    log_level = logging.CRITICAL 
elif const.env('LOG_LEVEL') == 'ERROR':
    log_level = logging.ERROR  
elif const.env('LOG_LEVEL') == 'WARNING':
    log_level = logging.WARNING  
elif const.env('LOG_LEVEL') == 'INFO':
    log_level = logging.INFO  
elif const.env('LOG_LEVEL') == 'DEBUG':
    log_level = logging.DEBUG  

logging.basicConfig(level=log_level)


# starts click
cli.cli(obj={})