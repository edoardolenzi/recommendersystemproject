#!/usr/bin/env python3
# coding: utf-8

'''Dataset importing tool'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import logging
import ast
import csv
import os
import json
import pandas as pd

from rsproject.commons import constants as const
from rsproject.commons.db_manager import DBManager
from rsproject.commons import localizations as loc


class Importer():


    '''Dataset importing tool'''
    

    LOGGER = logging.getLogger(__name__)
    __db_engine = None
    __db_manager = None

    # input files
    __file_csv_ratings = None
    __file_json_reviews = None
    __file_json_items = None

    # output files
    __o_csv_reviews = const.O_CSV_REVIEWS_PATH
    __o_csv_items = const.O_CSV_ITEMS_PATH
    __o_csv_categories = const.O_CSV_CATEGORIES_PATH
    __o_csv_item_category = const.O_CSV_ITEMS_CATEGORIES_PATH
    __o_csv_selected_items = const.O_CSV_ITEMS_SELECTED_PATH
    __o_csv_selected_users = const.O_CSV_USERS_SELECTED_PATH


    def __init__(self):
        # creates a DB connection
        self.__db_engine = DBManager().get_engine()
        self.__db_manager = DBManager()

        # setup input files (see the .env declarations)
        if const.env('DATA') == 'CHUNK':
            self.__file_json_reviews = const.CHUNK_REVIEWS
            self.__file_csv_ratings = const.CHUNK_RATINGS
            self.__file_json_items = const.CHUNK_ITEMS
        elif const.env('DATA') == 'RAW':
            self.__file_json_reviews = const.RAW_REVIEWS
            self.__file_csv_ratings = const.RAW_RATINGS
            self.__file_json_items = const.RAW_ITEMS
        else:
            raise Exception(loc.MISSING_DATA_KEY)   


    '''
    Stores on file the .json serialization of an object
    '''

    def serialize(self, obj: object, file: str) -> None:
        with open(file, 'w') as fp:
            json.dump(obj, fp)


    '''
    Deserializes the json contained in the file path
    '''

    def deserialize(self, file: str) -> object:
        with open(file, 'r') as fp:
            return json.load(fp)


    '''
    Takes a (comma separated} .csv file and allocates the corresponding table into the DB
    Note that the first line of the .csv must contains the column names 

    Args:
        csv_file: The input .csv file.
        table_name: The DB table name.
    '''

    def csv_to_table(self, csv_file: str, table_name: str) -> None:
        i = 0
        j = 1
        for df in pd.read_csv(csv_file, iterator=True):
            df = df.rename(columns={c: c.replace(' ', '') for c in df.columns}) 
            df.index += j
            i+=1

            # converts and launches the query
            df.to_sql(table_name, self.__db_engine, if_exists='append')
            try:
                j = df.index[-1] + 1
            except:
                self.LOGGER.error('Empty csv file {0}'.format(csv_file))
            # deletes the source .csv file (see .env declarations)
        if const.env('REMOVE_OUTPUT') == 'TRUE' and csv_file != self.__file_csv_ratings:
            os.remove(csv_file)
        self.LOGGER.info('Table \'' + table_name + '\' created from .csv!')


    '''
    Converts a .json un-nested (flats nesting to a string representation) file to .csv format  

    Args:
        json_file: The input .json file.
        csv_file: The output .csv file.
    '''

    def json_to_csv(self, json_file: str, csv_file: str) -> None:
        with open(csv_file, mode='w') as write_file:
            with open(json_file, 'r') as f:
                # reads one line in order to extract fieldnames
                fieldnames = ast.literal_eval(f.readline()).keys() 
                writer = csv.DictWriter(write_file, fieldnames=fieldnames)
                writer.writeheader()
                
                # resets the file index to 0
                f.seek(0)
                for line in f:
                    record = ast.literal_eval(line)
                    writer.writerow(record)

        self.LOGGER.info('Json converted to csv successfully')


    '''
    Creates the categories csv file from the .json containing the items
    '''
    
    def __create_categories_csv(self) -> None:
        rows = []
        counter = 0
        # salvo tutte le diverse categorie presenti nei vari prodotti
        with open(self.__file_json_items, 'r') as ins:
            for line in ins:
                parsed = ast.literal_eval(line)
                for elem in parsed['categories'][0]:
                    if not(elem in rows):
                        rows.append(elem)
      
        # creo il csv associando ad ogni categoria un id (intero sequenziale)
        with open(self.__o_csv_categories, mode='w') as write_file:
            fieldnames = ['id', 'category']
            writer = csv.DictWriter(write_file, fieldnames=fieldnames)
            writer.writeheader()
            for elem in rows:
                counter = counter + 1
                writer.writerow({'id': counter, 'category': elem})


    '''
    Creates a .csv that represents the schema of the join table needed for the 
    many to many relationship between items and categories from the .json file 
    containing the items
    '''

    def __create_many_to_many_csv(self) -> None:
        # inserimento di tutte le categorie in un dizionario
        with open(self.__o_csv_categories, 'r') as reader_file:
            reader = csv.reader(reader_file)
            categories = {}
            for k, v in reader:
                categories[v] = k
        
        # creo il csv con l'associazione prodotto-categoria, per ogni categoria
        with open(self.__o_csv_item_category, mode='w') as write_file:
            fieldnames = ['category', 'item']
            writer = csv.DictWriter(write_file, fieldnames=fieldnames)
            writer.writeheader()
            with open(self.__file_json_items, 'r') as ins:
                for line in ins:
                    parsed = ast.literal_eval(line)
                    # scrivo una nuova riga nel csv con l'associazione prodotto - categoria
                    for elem in parsed['categories'][0]:
                        writer.writerow({'category': categories[elem], 'item': parsed['asin']})


    '''
    Creates a .csv that represents the schema of the item table from the .json file 
    containing the items
    '''

    def __create_item_csv(self) -> None:
        with open(self.__o_csv_items, 'w') as write_file:
            fieldnames = ['id', 'asin', 'description', 'title', 'price']
            writer = csv.DictWriter(write_file, fieldnames=fieldnames)
            writer.writeheader()
            i = 0
            with open(self.__file_json_items, 'r') as ins:
                for line in ins:
                    parsed = ast.literal_eval(line)
                    d = {}
                    for l in fieldnames:
                        d[l] = '' if l not in parsed else str(parsed[l])
                    d['id'] = i
                    i += 1
                    writer.writerow(d)

        self.LOGGER.info('Item csv created!')


    '''
    Updates tables schema, joins with other tables, delete native ids (varchar) 
    in order to use internal ids (integer)  
    '''

    def tidy_tables(self) -> None:
        # creates users table
        self.__db_manager.execute('CREATE TABLE users as SELECT user, count(*) as numberOfRatings FROM ratings group by user')
        self.__db_manager.rename_and_index_table('users', 'SELECT * FROM users')

        # updates categories
        self.__db_manager.rename_and_index_table('categories', 'SELECT category FROM categories')

        # updates items_categories
        self.__db_manager.rename_and_index_table('items_categories', '''
            SELECT ic.category as category, i.id as item 
            FROM items i, items_categories ic 
            WHERE ic.item = i.asin''')

        # updates ratings (join with items and users)
        self.__db_manager.rename_and_index_table('ratings', '''
            SELECT u.id as user, i.id as item, r.rating, r.unixReviewTime 
            FROM ratings r, items i, users u 
            WHERE r.item = i.asin and r.user = u.user''')

        # updates reviews (join with items and users)
        self.__db_manager.rename_and_index_table('reviews', '''
            SELECT r.summary, r.reviewTime, r.unixReviewTime, r.reviewerName as userName, u.id as user, r.reviewText, r.overall, i.id as item, r.helpful 
            FROM reviews r, items i, users u  
            WHERE r.asin = i.asin and r.reviewerID = u.user''')
        
        # updates ratings (join with reviews)
        self.__db_manager.rename_and_index_table('ratings', '''
            SELECT rat.user, rat.item, rat.rating, rat.unixReviewTime, rev.id as review 
            FROM ratings rat, reviews rev  
            WHERE rat.unixReviewTime = rev.unixReviewTime and rev.user = rat.user and rat.item = rev.item''')

        # updates items 
        self.__db_manager.rename_table('items', 'SELECT id, description, title, price FROM items')

        # compact DB
        self.__db_manager.vacuum()


    '''
    Creates some support tables:
        selected_users
        selected_items
        selected_ratings
    '''

    def materialize_projections(self) -> None:
        # delete old tables
        try:
            self.__db_manager.execute('DROP TABLE selected_users')
            self.__db_manager.execute('DROP TABLE selected_items')
            self.__db_manager.execute('DROP TABLE selected_ratings')
        except:
            pass


        for i in range(0, 11):
            try:
                self.__db_manager.execute('DROP TABLE selected_users{0}'.format(i))
                self.__db_manager.execute('DROP TABLE selected_ratings{0}'.format(i))
            except:
                pass

        self.__db_manager.vacuum()
        self.LOGGER.debug('vacuum complete')

        for i in range(0, 11):
            self.__db_manager.materialize('selected_users{0}'.format(i), '''
                SELECT id as user
                FROM users 
                WHERE numberOfRatings > {0}'''.format(i))
            self.LOGGER.debug('selected_users{0} created'.format(i))

            self.__db_manager.materialize('selected_ratings{0}'.format(i), '''
                SELECT su.id as user, su.user as originalUser, r.item, r.rating 
                FROM ratings r, selected_users{0} su 
                WHERE r.user = su.user'''.format(i))
            self.LOGGER.debug('selected_ratings{0} created'.format(i))


        self.__db_manager.materialize('selected_users', '''
            SELECT id as user
            FROM users 
            WHERE numberOfRatings > {0}'''.format(const.env('RATING_TRASHOLD')))
        self.LOGGER.debug('selected_users created')

        self.__db_manager.materialize('selected_ratings', '''
            SELECT su.id as user, su.user as originalUser, r.item, r.rating 
            FROM ratings r, selected_users su 
            WHERE r.user = su.user''')
        self.LOGGER.debug('selected_ratings created')

        self.__db_manager.materialize('selected_items', '''
            SELECT id as item, description 
            FROM items 
            WHERE description IS NOT NULL''')
        self.LOGGER.debug('selected_items created')
             

    '''
    CLI entry point, launches the importing procedure
    '''
       
    def launch_import(self) -> None:
        try:
            os.remove(const.SQLITE_DB)
        except:
            pass
        self.__create_item_csv()
        self.__create_categories_csv()
        self.__create_many_to_many_csv()
        self.csv_to_table(self.__o_csv_categories, 'categories')
        self.csv_to_table(self.__o_csv_item_category, 'items_categories')
        self.csv_to_table(self.__o_csv_items, 'items')
        self.csv_to_table(self.__file_csv_ratings, 'ratings')
        if const.env('IMPORT_REVIEWS') == 'TRUE':
            self.json_to_csv(self.__file_json_reviews, self.__o_csv_reviews)
            self.csv_to_table(self.__o_csv_reviews, 'reviews')
        self.tidy_tables()

