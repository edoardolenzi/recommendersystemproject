#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Text preprocessing tool'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import Normalizer
from tqdm import tqdm
import numpy as np
import math
import warnings
import logging 

from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
import rsproject.commons.constants as const


@for_all_methods(timer)
class TextPreprocessing():


    __ps = None
    __stop_words = None
    LOGGER = logging.getLogger(__name__)


    def __init__(self):
        self.LOGGER.info('Preprocessing..')
        # init Stemmer (with Porter algorithm)
        self.__ps = PorterStemmer()
        # init stopwords set
        self.__stop_words = set(stopwords.words('english'))


    '''
    Prints the selected metric

    Args:
        to_print
    '''

    def __print_measure(self, to_print: str) -> None:
        width = len(to_print) + 30
        self.LOGGER.info(to_print.center(width, '-'))


    '''
    Removes the stopwords
    
    Args:
        to_preprocess

    Returns:
        Array of tokens
    '''

    def stopwords_removal(self, to_preprocess: str) -> []:
        filtered_sentence = []
        example_sent = to_preprocess
        # regex for the special characters 
        tokenizer = RegexpTokenizer(r'\w+')
        try:
            # split the list and remove the special characters
            word_tokens = tokenizer.tokenize(example_sent.lower())
            filtered_sentence = [w for w in tqdm(word_tokens, total=len(word_tokens), desc='stop_word', disable=bool(int(const.env('TQDM')))) if not w in self.__stop_words]  
        except:
            pass
        return filtered_sentence


    '''
    Stemming function

    Args:
        to_preprocess

    Returns:
        Array of tokens
    '''

    def stemming(self, to_preprocess: []) -> []:
        return [self.__ps.stem(w) for w in to_preprocess]
        

    '''
    Bag of words

    Args:
        to_preprocess_list

    Returns:
        Dictionary {(token, token-frequency)}
    '''

    def bag_of_words(self, to_preprocess_list: []) -> dict:
        bow = {}
        for i in to_preprocess_list:
            if i not in bow:
                bow[i] = 1
            else:
                bow[i] += 1
        return bow


    '''
    Dice similarity

    Args:
        user1: feature vector
        user2: feature vector

    Returns:
        The similarity value (numeric)
    '''
    
    def dice(self, user1: [], user2: []) -> int:
        user1_set = set(user1)
        user2_set = set(user2)
        user1_len = len(user1)
        user2_len = len(user2)
        len_intersection = len(user1_set.intersection(user2_set))
        return (2 * len_intersection) / (user1_len + user2_len) if user1_len != 0 and user2_len != 0 else 0
    

    '''
    Jaccard similarity

    Args:
        user1: feature vector
        user2: feature vector

    Returns:
        The similarity value (numeric)
    '''

    def jaccard(self, user1: [], user2: []) -> int:
        user1_set = set(user1)
        user2_set = set(user2)
        len_intersection = len(user1_set.intersection(user2_set))
        len_union = len(user1_set.union(user2_set))
        return len_intersection / len_union if len_union != 0 else 0


    '''
    Cosine similarity

    Args:
        user1: feature vector
        user2: feature vector

    Returns:
        The similarity value (numeric)
    '''
    
    def cosine(self, user1: [], user2: []) -> int:
        user1_set = set(user1)
        user2_set = set(user2)
        user1_len = len(user1)
        user2_len = len(user2)
        len_intersection = len(user1_set.intersection(user2_set))
        i = math.sqrt(user1_len)
        j = math.sqrt(user2_len)
        return len_intersection / (i * j) if i != 0 and j != 0 else 0


    '''
    Dice on a list

    Args:
        index: pivot index
        to_analize: list of items

    Returns:
        Vector of tuples (code, similarity)
    '''

    def dice_list(self, index: int, to_analize: []) -> []:
        data = []
        for (cod, desc) in tqdm(to_analize, total=len(to_analize), desc='dice', disable=bool(int(const.env('TQDM')))):
            res = self.dice(to_analize[index], desc)
            data.append((cod, res))
        data.sort(key=lambda tup: tup[1], reverse=True)
        return data
    

    '''
    Jaccard on a list

    Args:
        index: pivot index
        to_analize: list of items

    Returns:
        Vector of tuples (code, similarity)
    '''

    def jaccard_list(self, index: int, to_analize: []) -> []:
        data = []
        for (cod, desc) in tqdm(to_analize, total=len(to_analize), desc='jaccard', disable=bool(int(const.env('TQDM')))):
            res = self.jaccard(to_analize[index], desc)
            data.append((cod, res))
        data.sort(key=lambda tup: tup[1], reverse=True)
        return data
    

    '''
    Cosine on a list

    Args:
        index: pivot index
        to_analize: list of items

    Returns:
        Vector of tuples (code, similarity)
    '''

    def cosine_list(self, index: int, to_analize: []) -> []:
        data = []
        for (cod, desc) in tqdm(to_analize, total=len(to_analize), desc='cosine', disable=bool(int(const.env('TQDM')))):
            res = self.cosine(to_analize[index], desc)
            data.append((cod, res))
        data.sort(key=lambda tup: tup[1], reverse=True)
        return data
    
        
    '''
    Auxiliar function for the creation fo the tf*idf matrix

    Args:
        docs: array of documents

    Returns:
        Matrix
    '''

    def tfidf_matrix(self, docs: []) -> object:
        vectorizer = TfidfVectorizer()
        response = vectorizer.fit_transform(docs)
        return response


    '''
    Function for computation of |A| (see the slides)

    Args:
        doc: document index
        mtx: matrix

    Returns:
        |A|
    '''

    def __set_lenght(self, doc: int, mtx: object) -> int:
        doc-=1
        tot = 0
        for i in mtx[doc, :].nonzero()[1]:
            tot += mtx[doc, i] * mtx[doc, i]
        return math.sqrt(tot)
    

    '''
    Function for the set product

    Args:
        doc1: first document index
        doc2: second document index
        mtx: matrix

    Returns:
        set product (sum of the product of shared ratings)
    '''
    
    def __set_product(self, doc1: int, doc2: int, mtx: object) -> int:
        doc1_set = set(mtx[doc1, :].nonzero()[1])
        doc2_set = set(mtx[doc2, :].nonzero()[1])
        val = 0
        for i in doc1_set.intersection(doc2_set):
            val += mtx[doc1, i] * mtx[doc2, i]
        return val


    '''
    Casts list of strings to string

    Args:
        docs: list of strings

    Returns:
        a string
    '''

    def __from_list_to_strings(self, docs: []) -> str:
        return [' '.join(str(d) for d in _doc) for _doc in tqdm(docs, total=len(docs), desc='from_list_to_string', disable=bool(int(const.env('TQDM'))))]


    '''
    Computes the similarity between items using consie similarity

    Args:
        doc_index: pivot index
        docs: list of documents

    Returns:
        Matrix
    '''

    def tfidf_cosine(self, doc_index: int, docs: []) -> object:
        self.__print_measure('TFIDF for {}'.format(doc_index))

        # Converts list of strings to strings
        documents = self.__from_list_to_strings(docs)

        # computes tfidf matrix
        mtx = self.tfidf_matrix(documents)
        data = []

        # Apply cosine only on the first n items (reduces the computation time)
        # TODO launch on different threads (work in progress)
        num1 = self.__set_lenght(doc_index, mtx)
        for i in tqdm(range(mtx.shape[0]), desc='Cosine', disable=bool(int(const.env('TQDM')))):
            num2 = self.__set_lenght(i, mtx)
            if num1 != 0 and num2 != 0:
                den = self.__set_product(doc_index, i, mtx)
                data.append((i, den / (num1 * num2)))
            else:
                data.append((i, 0))

        # sort in descending order
        data.sort(key=lambda tup: tup[1], reverse=True)
        return data[0:5]


    '''
    Computes the similarity between items using consine similarity

    Args:
        doc_index: pivot index
        docs: list of documents

    Returns:
        Matrix
    '''

    def lsi(self, docs: [], index: int, tfidf: bool = True) -> object:
        self.__print_measure('LSI for {}'.format(index))
        warnings.filterwarnings('ignore', category=DeprecationWarning, module='pandas', lineno=570)
        docu = self.__from_list_to_strings(docs)

        # if tfidf == True then uses tdidf else uses the occurrences matrix
        vectorizer = TfidfVectorizer() if tfidf else CountVectorizer()
        dtm = vectorizer.fit_transform(docu)

        # computes svd on n_comp > 100 if the number of items is greater than 500, 30 otherwise 
        lsa = TruncatedSVD(n_components=100 if len(docs) > 500 else 30, algorithm='randomized')
        dtm_lsa = lsa.fit_transform(dtm)
        dtm_lsa = Normalizer(copy=False).fit_transform(dtm_lsa)

        # multiply the rows of the selected item with the rest of the matrix
        similarity = np.asarray(np.asmatrix(dtm_lsa[index]) * np.asmatrix(dtm_lsa).T)
        count = 0
        data = []

        # build the output list
        for i in tqdm(similarity[0], total=len(similarity[0]), desc='building_list', disable=bool(int(const.env('TQDM')))):
            data.append((count, i))
            count += 1
        data.sort(key=lambda tup: tup[1], reverse=True)
        return data[0:6]