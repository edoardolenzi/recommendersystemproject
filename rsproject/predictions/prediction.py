#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Predictions module
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from sqlalchemy import create_engine
from sqlalchemy.sql import select
import scipy.sparse as sps
import logging
from tqdm import tqdm

from rsproject.commons.db_manager import DBManager
from rsproject.similarities.cosine_similarity import CosineSimilarity
from rsproject.similarities.mse import MSE
from rsproject.similarities.pearson_similarity import PearsonSimilarity
from rsproject.similarities.proximity_measure import ProximityMeasure
from rsproject.similarities.mean_adjusted_cosine_similarity import MeanAdjustedCosineSimilarity
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.matrix import Matrix
import rsproject.commons.constants as const


class Prediction():


    __matrix = None
    __mtx = None
    __index = None
    __db_manager = None
    __adjusted_cosine_similarity = None
    LOGGER = logging.getLogger(__name__)


    def __init__(self, matrix: Matrix, index: str):
        self.__matrix = matrix
        self.__mtx = SparseMatrix().import_matrix(matrix)
        self.__index = index
        self.__db_manager = DBManager()
        self.__adjusted_cosine_similarity = MeanAdjustedCosineSimilarity(
            Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
    

    def user_based_prediction(self, item_index, measure_data):
        guide = []
        count = 0
        for (k, v) in measure_data:
            if self.__mtx.matrix[k, item_index] != 0:
                guide.append((k, v))
                count += 1
            if count == int(const.env('N_GUIDES')):
                break
        if count != 0:
            # calcolo la media dei voti di ogni utente
            means = {}
            user_mean = self.get_mean(self.__index)
            for (k, v) in guide:
                m = self.get_mean(k)
                means[k] = m

            num = 0
            den = 0

            for (k, v) in guide:
                den += v
                num += v * (self.__mtx.matrix[k, item_index] - means[k])
            
            return user_mean + (num / den) if den != 0 else 0.00
        else:
            return 0.00


    def get_mean(self, user):
        return sum(self.__mtx.matrix[user, :].toarray()[0]) / self.__mtx.matrix[user, :].count_nonzero()


    def print_prediction(self, metric, item, value):
        self.LOGGER.info('({}) Previsione su item {}: {}'.format(metric, item, value))


    def get_user_based_predictions_random_items(self, metrica_name, user_simil):
        self.LOGGER.info('GET PREDICTION ON USER {}, MEAN: {}'.format(self.__index, self.get_mean(self.__index)))
        
        user_asin = self.__db_manager.execute('SELECT codUser FROM selected_users WHERE codUser={}'.format(self.__index))
        not_buied_items = self.__db_manager.execute('''
            SELECT COUNT(*) AS counter, item, codItem 
            FROM ratings 
            JOIN items ON item=item 
            WHERE item NOT IN (
                SELECT item 
                FROM ratings 
                WHERE user={}
            ) 
            GROUP BY item 
            ORDER BY counter DESC LIMIT {}'''.format([x for x in user_asin][0][0], int(const.env('TO_PREDICT_ITEM'))))
        
        for (counter, item, codItem) in not_buied_items:
            self.print_prediction(metrica_name, item, self.user_based_prediction(codItem, user_simil))


    def predict_random_items(self, metric):
        if metric.lower() == 'similarity':
            self.get_user_based_predictions_random_items('Similarity', ProximityMeasure(self.__matrix).execute(self.__index))
        if metric.lower() == 'mse':
            self.get_user_based_predictions_random_items('MSE', MSE(self.__matrix).execute(self.__index))
        if metric.lower() == 'cosine':
            self.get_user_based_predictions_random_items('Cosine', CosineSimilarity(self.__matrix).execute(self.__index))
        if metric.lower() == 'pearson':
            self.get_user_based_predictions_random_items('Pearson', PearsonSimilarity(self.__matrix).execute(self.__index))


    '''
    Args:
        measure: vector of tuples (user, id)
    '''
    
    def predict_for_training(self, measure, item):
        return self.user_based_prediction(item, measure)


    # il voto che daresti all'item item_index dato user_index (matrice sbiancata)
    def predict_for_training_item_based(self, user_index, item_index):
        user_row = self.__matrix.matrix[user_index, :].todense()
        
        non_zero_item_indexes = [i for i, c in enumerate(user_row.tolist()[0]) if c != 0]
        metric = MeanAdjustedCosineSimilarity(self.__matrix)

        num = den = 0

        for item_guide in tqdm(non_zero_item_indexes, total = len(non_zero_item_indexes)):
            sim = metric.auxiliar(item_index, item_guide)[1] 
            num += sim * int(self.__matrix.matrix[user_index, item_guide])
            den += sim 
        
        return num / den if den != 0 else 0
