#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Cosine similarity'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from datetime import datetime
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import cosine_similarity
import torch 
import numpy as np
from tqdm import tqdm
import os
import scipy

from rsproject.importer.importer import Importer
from rsproject.similarities.base_similarity import BaseSimilarity
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.similarities.cosine_similarity import CosineSimilarity
import rsproject.commons.constants as const  


@for_all_methods(timer)
class MeanAdjustedCosineSimilarity(BaseSimilarity):


    '''Mean Adjusted Cosine similarity on a sparse matrix |users| x |products| with the ratings'''


    __user_means =  None
    __cosine = None


    def __init__(self, matrix: Matrix):
        BaseSimilarity.__init__(self, matrix)
        sparse_mtx = SparseMatrix() 
        if not os.path.exists(const.MEAN_ADJUSTED_COSINE_MATRIX_PATH):
            Importer().serialize(self.convert_matrix(), const.MEAN_ADJUSTED_COSINE_MATRIX_PATH)
        self.__user_means = Importer().deserialize(const.MEAN_ADJUSTED_COSINE_MATRIX_PATH)
        self.__cosine = CosineSimilarity(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))


    def execute(self, index: int) -> []:
        self.print_measure('Mean Adjusted Cosine similarity')
        le = np.shape(self._mtx.matrix)[1] # cycle on columns
        data = []
        for i in tqdm(range(0, le)):
            if i != index :
                sim = self.auxiliar(i, index)
                data.append((i, sim[0], sim[1]))
        data.sort(key=lambda tup: tup[1], reverse=True)
        data = self.order_tuple(data)
        self.LOGGER.debug(data)
        self.common_users(data, 'Mean Adjusted Cosine', index)
        print(data[0:10])
        return data

    # computes the similarity for the columns

    def auxiliar(self, index1: int, index2: int) -> int:
        commons = self.common_user(index1, index2)
        if len(commons) != 0:
            return (self.__cosine.cosine(commons[0], commons[1]), len(commons[0]))
        return (0, 0)


    def convert_matrix(self):
        #mtx = np.array([
        #    [5, 3, 4, 4, 0],
        #    [3, 1, 2, 3, 3],
        #    [4, 3, 4, 3, 5],
        #    [3, 3, 1, 5, 4],
        #    [1, 5, 5, 2, 1],
        #])
        result = []

        for row in tqdm(self._mtx.matrix, total=np.shape(self._mtx.matrix)[0]):
            row = np.array(row.todense())
            mean = np.mean(row)
            result.append(mean)
        return result



    def common_user(self, item_di_user, altro_item):
        i = set(self._mtx.matrix[:,item_di_user].nonzero()[0])
        j = set(self._mtx.matrix[:,altro_item].nonzero()[0])
        inter = i.intersection(j)
        primo = []
        secondo = []
        for k in inter:
            primo.append(self._mtx.matrix[k,item_di_user] - self.__user_means[k])
            secondo.append(self._mtx.matrix[k,altro_item] - self.__user_means[k])
        return [primo, secondo]


#    def mean_adjusted_cosine(self, index1: int, index2: int):
#        mean_a = self.__user_means[index1]
#        mean_b = self.__user_means[index2]
#
#        a = np.array(a)
#        b = np.array(b)
#        return sum(a * b)/(np.linalg.norm(a) * np.linalg.norm(b))
#        return [] if len(inter) == 0 else [primo,secondo]