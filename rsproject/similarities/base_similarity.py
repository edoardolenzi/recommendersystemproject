#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Base class for the similarity measures for CF'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import logging
import abc
from sklearn.preprocessing import normalize as normalize_matrix

from rsproject.commons.db_manager import DBManager
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.matrix import Matrix
import rsproject.commons.constants as const


class BaseSimilarity(abc.ABC):


    ''' 
    Base abstract class for similarity measures;
    imposes the implamentation of the methods decorated by @abc.abstractmethod
    '''
    

    _mtx = None
    __db_manager = None
    __users_dict = {}
    LOGGER = logging.getLogger(__name__)


    def __init__(self, matrix: Matrix):
        self.__db_manager = DBManager()
        self.LOGGER.info('Connesso al db')
        self._mtx = SparseMatrix().import_matrix(matrix)


    '''
    Method signature, execute on the entire matrix a 
    similarity measure wrt a pivot user 

    Args: 
        index: index of the pivot user

    Returns:
        array with the similarities
    '''

    @abc.abstractmethod
    def execute(self, index: int) -> []:
        pass


    '''
    Method signature, execute the similarity measure on two users

    Args: 
        index1: index of the first user
        index2: index of the second user

    Returns:
        array with the similarities
    '''

    @abc.abstractmethod
    def auxiliar(self, index1: int, index2: int) -> int:
        pass


    '''
    Given two matrix row indices returns the common/shared items rated by both users 

    Args: 
        index1: index of the first user
        index2: index of the second user

    Returns:
        an array of two arrays containing the ratings of the two users
    '''

    def get_commons(self, index1: int, index2: int, normalize: bool = False) -> []:
        mtx = self._mtx.matrix
        if normalize:
            mtx = normalize_matrix(mtx)
        i = set(mtx[index1, :].nonzero()[1])
        j = set(mtx[index2, :].nonzero()[1])
        inter = i.intersection(j)
        common_items_user1 = []
        common_items_user2 = []
        for k in inter:
            common_items_user1.append(mtx[index1, k])
            common_items_user2.append(mtx[index2, k])
        q = [common_items_user1, common_items_user2]
        return q if len(inter) != 0 else []


    '''
    Formats and logs an input string 
    '''

    def print_measure(self, to_print: str) -> None:
        width = len(to_print) + 30
        self.LOGGER.info(to_print.center(width, '-'))


    '''
    Prints the 3 users more similar to the pivot user and the common items
    '''

    def common_users(self, data: [], tecnique: str, index: int) -> None:
        for (i, j) in data[0:4]: 

            user_code = self.__db_manager.execute('''select u.user from selected_users su, users u 
                where su.user = u.id and su.id = {0}'''.format(i))
            user_code = [x for x in user_code][0][0]

            print('Utente {0} (id mappato {1}) similarity {2}'.format(user_code, i, j))

            items = self.__db_manager.execute('''
                select item from selected_ratings where user = {0}  and item in 
                (select item from selected_ratings where user = {1})'''.format(index, i))
            k = 0
            for item in items:
                k += 1
            #    print(item[0])
            print('In comune: {}'.format(k))


    '''
    Given an array of tuples, order by the second component wrt the first (just sorted)
    '''

    def order_tuple(self, data):
        temp = []
        output = []
        tmp_value = data[0][1]
        
        for (user, value, number) in data:
            if value == tmp_value:
                temp.append((user, value, number))
            else:
                tmp_value = value
                temp.sort(key=lambda tup: tup[2], reverse=True)
                for (i,j,k) in temp:
                    output.append((i,j))
                temp = [(user,value,number)]
        
        temp.sort(key=lambda tup: tup[2], reverse=True)
        for (i,j,k) in temp:
            output.append((i,j))
        return output