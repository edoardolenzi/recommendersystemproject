#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Pearson similarity'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from sklearn.preprocessing import normalize
import numpy as np
from datetime import datetime
from scipy import stats
from tqdm import tqdm

from rsproject.similarities.base_similarity import BaseSimilarity
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix


@for_all_methods(timer)
class PearsonSimilarity(BaseSimilarity):
    
    
    ''' 
    Pearson similarity on normalized matrix
    '''
    

    def __init__(self, matrix: Matrix):
        BaseSimilarity.__init__(self, matrix)
    

    def execute(self, index: int) -> []:
        self.print_measure('Pearson similarity')
        le = np.shape(self._mtx.matrix)[0]
        data = []
        # normalizzo la matrice
        mat = normalize(self._mtx.matrix)
        self.LOGGER.debug('Inizio pearson {}'.format(datetime.now()))
        mat = mat.tolil()
        for i in tqdm(range(0, le), total=le):
            if i != index:
                sim = self.auxiliar(index, i)
                data.append((i, sim[0], sim[1]))
        self.LOGGER.debug('fine pearson {}'.format(datetime.now()))
        data.sort(key=lambda tup: tup[1], reverse=True)
        data = self.order_tuple(data)
        self.common_users(data, 'Pearson', index)
        return data

    
    def auxiliar(self, index1: int, index2: int) -> int:
        common_items = self.get_commons(index1, index2)
        if len(common_items) != 0:
            return (stats.pearsonr(common_items[0], common_items[1])[0], len(common_items[0]))
        return (0, 0)

    def pearson(self, a: [], b: []) -> int:
        #a = [5,3,4,4]
        #b = [3,1,2,3]
        return stats.pearsonr(a, b)[0]
