#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''MSE Similarity'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from sklearn.metrics import mean_squared_error
import numpy as np
from tqdm import tqdm

from rsproject.similarities.base_similarity import BaseSimilarity
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix


@for_all_methods(timer)
class MSE(BaseSimilarity):


    ''' 
    Scarto quadratico medio (https://it.wikipedia.org/wiki/Scarto_quadratico_medio)
    Risultato basso sim basso → poche differenze → somiglianza alta! 
    '''


    def __init__(self, matrix: Matrix):
        BaseSimilarity.__init__(self, matrix)


    def execute(self, index: int) -> []:
        self.print_measure('Scarto quadratico')
        le = np.shape(self._mtx.matrix)[0]
        data = []
        for i in tqdm(range(0, le), total=le):
            if i != index:
                sim = self.auxiliar(i, index)
                data.append((i, sim[0], sim[1]))
        data.sort(key=lambda tup: tup[1], reverse=False)
        data = self.order_tuple(data)
        self.LOGGER.debug(data)
        self.common_users(data, 'MSE', index)
        return data


    def auxiliar(self, index1: int, index2: int) -> int:
        common_items = self.get_commons(index1, index2)
        return (100, 0) if len(common_items) == 0 else (mean_squared_error(common_items[0], common_items[1]), len(common_items[0]))

   

