#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Cosine similarity'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from datetime import datetime
from sklearn.preprocessing import normalize
from sklearn.metrics.pairwise import cosine_similarity
import torch 
import numpy as np
from tqdm import tqdm

from rsproject.similarities.base_similarity import BaseSimilarity
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix


@for_all_methods(timer)
class CosineSimilarity(BaseSimilarity):


    '''Cosine similarity on a sparse matrix |users| x |products| with the ratings'''
    # Vale al max. 1 se la similarità è massima e al min. 0 se i due item appaiono indipendenti


    def __init__(self, matrix: Matrix):
        BaseSimilarity.__init__(self, matrix)


    def execute(self, index: int) -> []:
        self.print_measure('Cosine similarity')
        le = np.shape(self._mtx.matrix)[0]
        data = []
        for i in tqdm(range(0, le)):
            if i != index :
                sim = self.auxiliar(i, index)
                data.append((i, sim[0], sim[1]))
        data.sort(key=lambda tup: tup[1], reverse=True)
        data = self.order_tuple(data)
        self.LOGGER.debug(data)
        self.common_users(data, 'Cosine', index)
        print(data[0:10])
        return data


    def auxiliar(self, index1: int, index2: int) -> int:
        common_items = self.get_commons(index1, index2)
        if len(common_items) != 0:
            return (self.cosine(common_items[0], common_items[1]), len(common_items[0]))
        return (0, 0)


    def cosine(self, a, b):
        #a = [3, 4, 3, 1]
        #b = [3, 5, 4, 1]
        a = np.array(a)
        b = np.array(b)
        return sum(a * b)/(np.linalg.norm(a) * np.linalg.norm(b))