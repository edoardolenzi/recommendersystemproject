#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''Proximity measure'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import numpy as np
import math
from tqdm import tqdm

from rsproject.similarities.base_similarity import BaseSimilarity
from rsproject.decorators.for_all_methods import for_all_methods
from rsproject.decorators.timer import timer
from rsproject.commons.models.matrix import Matrix


@for_all_methods(timer)
class ProximityMeasure(BaseSimilarity):
    
    
    ''' 
    Proximity measure
    '''


    def __init__(self, matrix: Matrix):
        BaseSimilarity.__init__(self, matrix)


    def execute(self, index):
        self.print_measure('Proximity measure')
        le = np.shape(self._mtx.matrix)[0]
        data = []
        for i in tqdm(range(0, le), total=le):
            if i != index:
                sim = self.auxiliar(index, i)
                data.append((i, sim[0], sim[1]))
        data.sort(key=lambda tup: tup[1], reverse=True)
        data = self.order_tuple(data)
        self.common_users(data, 'Proximity', index)
        return data   


    def auxiliar(self, index1, index2):
        common_items = self.get_commons(index1, index2)
        somma = 0
        if len(common_items) != 0:
           n = len(common_items[0])
           for i in range(0, n):
               somma +=  (5 - (common_items[0][i] - common_items[0][i]))
           return (0.00, 0) if n == 0 else ((somma / n) * math.log2(n), len(common_items[0]))
        return (0, 0)

