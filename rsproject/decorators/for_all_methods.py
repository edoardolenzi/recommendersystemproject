#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
for_all_methods is a simple class decorator
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


'''
for_all_methods is a simple class decorator that applies an input 
decorator to every method of the class

Args:
    decorator: another decorator method
'''

def for_all_methods(decorator):
    def decorate(cls):
        for attr in cls.__dict__: # there's propably a better way to do this
            if callable(getattr(cls, attr)):
                setattr(cls, attr, decorator(getattr(cls, attr)))
        return cls
    return decorate