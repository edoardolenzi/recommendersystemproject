#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
timer is a simple method decorator
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

import functools
import time
import logging

LOGGER = logging.getLogger(__name__)


'''
timer is a simple method decorator that logs the decorated method 
execution time when invocated
'''

def timer(func):
    '''Print the runtime of the decorated function'''
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        LOGGER.debug('Finished {0} in {1} secs'.format(func.__name__, run_time))
        return value
    return wrapper_timer