#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
sinleton is a simple class decorator
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


'''
sinleton is a simple class decorator that implements the singleton pattern 
(which is a part of the 'inversion of control' pattern)

The idea is that the decorated class returns, when instantiated, 
a unique shared instance
'''

def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance
