#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Hybrid Testing Functions
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'

from sqlalchemy import create_engine
from sqlalchemy.sql import select
from datetime import datetime
import scipy.sparse as sps
from tqdm import tqdm

from rsproject.content_based.text_preprocessing import TextPreprocessing
import rsproject.hybrid.user_similarity_functions as us


class HybridTestingFunctions():


    __mtx = None
    __user = None
    __db = None
    __users_dict = {}
    __items_dict = {}
    __preprocesser = None


    def __init__(self, user):
        print("Hybrid..")
        self.__user = user
        self.__db = self.__getDB()
        self.__preprocesser = TextPreprocessing()
        # creo il dizionario con i vari utenti
        all_users = self.__db.execute("SELECT codUser, user FROM selected_users")
        all_items = self.__db.execute("select s.codItem, asin from selected_items s join items i on s.codVecchio=i.codItem")
        for (k,v) in all_users:
            self.__users_dict[int(k)] = v
        for (k,v) in all_items:
            self.__items_dict[int(k)] = v
    

    def __getDB(self):
        engine = create_engine('sqlite:///csv_database.db')
        connection = engine.connect()
        return connection

    '''
    get common users with our user
    '''
    
    def __get_similar_users(self, name, mtx):
        if name == "pearson":
            return us.pearson(self.__user, mtx)
        if name == "proximity":
            return us.proximity_measure(self.__user, mtx)
    

    def __item_metric_factory(self, name, documents):
        if name == "lsi_tfidf":
            return self.__preprocesser.lsi(documents,len(documents)-1)
        if name == "lsi_count":
            return self.__preprocesser.lsi(documents,len(documents)-1, False)
        if name == "jaccard":
            return self.__preprocesser.jaccard_list(len(documents)-1, documents)
        if name == "cosine":
            return self.__preprocesser.cosine_list(len(documents)-1, documents)
        if name == "dice":
            return self.__preprocesser.dice_list(len(documents)-1, documents)


    def __get_month(self,data):
        if data.month >0 and data.month <= 3:
            month = 0
        if data.month > 3 and data.month <= 6:
            month = 1
        if data.month > 6 and data.month <= 12:
            month = 2
        return month


    def __create_period(self, data):
        #return "{}-{}".format(data.year,self.__get_month(data))
        a = "{}".format(data.year)
        return a


    '''
    check which user has bought an item in the same period
    '''

    def __check_timestamp_distance(self, similar_users, name, user_items):
        # calcolo item dell'user
        already_found = set()
        user_desc = []
        for (cod, desc, time) in user_items:
            already_found.add(cod)
            user_desc.append((cod, desc, time))

        time_dict = {}

        guides = ""
        for i in range(len(similar_users)):
            guides += " r.user =\'{}\' ".format(self.__users_dict[similar_users[i][0]])
            if i != len(similar_users) -1:
                guides += " OR "

        query = self.__db.execute("""
                SELECT i.codItem, i.description, r.timestamp
                FROM items i JOIN ratings r ON i.asin = r.asin
                WHERE ({}) AND i.description IS NOT NULL
                """.format(guides))
        
        # inserisco nel dizionario i documenti e i codici suddivisi per periodo
        # time_dict{
        #   '2005-01': {
        #       'descriptions': [desc1, desc2, desc3]
        #       'cods: [cod1, cod2, cod3]       
        #    }
        #   '2008-02': {
        #       'descriptions': [desc1, desc2, desc3]
        #       'cods: [cod1, cod2, cod3]       
        #    }
        # }
        for (cod,desc,time) in query:
            # se non e gia stato trovato (quindi anche gia comprato da user)
            if cod not in already_found:
                # trasformo timestamp in date
                data = datetime.fromtimestamp(int(time))
                # calcolo il codice a partire dall'anno e dal mese (blocchi di tre mesi)
                data_temp = self.__create_period(data)
                # se quel codice non e gia stato usato allora lo creo
                if data_temp not in time_dict:
                    time_dict[data_temp] = {}
                    # salvo la descrizione
                    time_dict[data_temp].update({"descriptions": [desc]})
                    # salvo il codice del prodotto corrispondente
                    time_dict[data_temp].update({"cods": [cod]})
                else:
                    time_dict[data_temp]["descriptions"].append(desc)
                    time_dict[data_temp]["cods"].append(cod)
                
                already_found.add(cod)

        # inserisco in un dizionario i documenti e i codici suddivisi per periodo per l'utente
        time_dict_user = {}
        for (cod, desc, time) in user_desc:
            data = datetime.fromtimestamp(int(time))
            data_temp = self.__create_period(data)
            if data_temp not in time_dict_user:
                time_dict_user[data_temp] = {}
                # salvo la descrizione
                time_dict_user[data_temp].update({"descriptions": [desc]})
                # salvo il codice del prodotto corrispondente
                time_dict_user[data_temp].update({"cods": [cod]})
            else:
                time_dict_user[data_temp]["descriptions"].append(desc)
                time_dict_user[data_temp]["cods"].append(cod)
        
        # prendo il periodo dell'utente con piu acquisti
        max_dict_value = 0
        max_dict_key = None
        for key in time_dict_user.keys():
            tmp = len(time_dict_user[key]["cods"])
            if tmp > max_dict_value and key in time_dict:
                max_dict_value = tmp
                max_dict_key = key
        return self.__type_one(time_dict, time_dict_user, max_dict_key, name)
        #return self.__type_two(time_dict, time_dict_user, max_dict_key, name)


    '''
    testing basato sui precedenti acquisti effettuati sia dall'utente e dalle guide
    avrebbe acquistato in quel periodo qualcosa che poi ha effettivamente aquistato?
    '''
    
    def __type_one(self, guides_items_dict, user_items_dict, key, name):
        # var di output
        output = []
        # salvo i docs di user
        user_descs_list = []

        # salvo i docs delle guide
        guides_descs_list = []

        #salvo i codici di user e guide
        user_cods_list = []
        guides_cods_list = []
        
        # mi salvo solo i docs e cods del periodo precedente
        for dict_key in user_items_dict.keys():
            if dict_key != key:
                for i in user_items_dict[dict_key]["descriptions"]:
                    user_descs_list.append(self.__preprocesser.stopwords_removal(i))
                for i in user_items_dict[dict_key]["cods"]:
                    user_cods_list.append(i)
            else:
                break
        
        for dict_key in guides_items_dict.keys():
            if dict_key != key:
                for i in guides_items_dict[dict_key]["descriptions"]:
                    guides_descs_list.append(self.__preprocesser.stopwords_removal(i))
                for i in guides_items_dict[dict_key]["cods"]:
                    guides_cods_list.append(i)
            else:
                break
        for i in range(len(guides_items_dict[key]["descriptions"])):
            guides_descs_list.append(self.__preprocesser.stopwords_removal(guides_items_dict[key]["descriptions"]))
            guides_cods_list.append(guides_items_dict[dict_key]["cods"][i])

        # scorro gli ultimi acquisti dell'utente nel periodo precedente
        quanti = len(user_descs_list)
        print(quanti)

        media = 0

        # per ogni item calcolo la similarita
        for i in range(quanti):
            docs = guides_descs_list[:]
            docs.append(user_descs_list[i])
            
            # calcolo la similarita
            res = self.__item_metric_factory(name,docs)[1:]
            
            # prendo ogni risultato dell'output e lo metto nella var output
            output = []
            for (cod, val) in res:
                try:
                    output.append((guides_cods_list[cod],val))
                except:
                    print("ECCEZIONE : {}".format(cod))
                    output.append((-1,val))

            # ordino tutto in base alla rilevanza
            output.sort(key=lambda tup: tup[1], reverse=True)
            guides_set = set([i for (i,j) in output[0:]])
            my_set = set(user_items_dict[key]["cods"])
            media += len(my_set.intersection(guides_set))
            print(media)
        
        # ritorno gli item in comune
        return media / len(user_items_dict[key]["cods"])
    

    '''
    testing basato sull'azzeramento di un item alla volta di un periodo
    vedendo gli acquisti effettuati dagli altri in quel periodo, avrebbe comprato quell'item?
    per ogni item I acquistato in quel periodo dal mio utente
      1. faccio la metrica di similarita con gli item acquistati dalle guide in quel periodo
      2. prendo gli n piu simili all'item I e vedo se qualcuno di quelli azzerati compare
    '''

    def __type_two(self, guides_items_dict, user_items_dict, key, name):
        
        # salvo i codici degli item acquistati nel periodo di riferimento
        user_cods = user_items_dict[key]['cods'][:]

        # lunghezza lista
        user_cods_len = len(user_cods)

        # variabile per la media finale
        media = 0

        # per ongi item di quel periodo..
        for i in range(user_cods_len):
        
            
            # tolgo l'i-esimo 
            #user_cods.pop(user_items_dict[key]["cods"][i])
            
            # creo un set con tutti gli altri codici
            tmp_val = user_items_dict[key]["cods"][i]
            total_items = set()
            total_items.add(tmp_val)
            
            docs = []
            docs_cods = []
            # per ogni item acquistato dalle quide
            guides_cods_len = len(guides_items_dict[key]["cods"])
            for cod in range(guides_cods_len):

                # se quell'item non e stato gia acquistato dall'utente lo aggiungo
                # ai documenti di confronto
                if guides_items_dict[key]["cods"][cod] not in total_items:
                    total_items.add(cod)
                    docs_cods.append(cod)
                    docs.append(self.__preprocesser.stopwords_removal(guides_items_dict[key]["descriptions"][cod]))
            
            # inserisco il documento sul quale devo effettuare il confronto
            docs.append(self.__preprocesser.stopwords_removal(user_items_dict[key]["descriptions"][i]))
            docs_cods.append(user_items_dict[key]["cods"][i])

            # calcolo la metrica
            tmp = self.__item_metric_factory(name,docs)[1:]
            print(len(tmp))
            # inserisco il risultato nella variabile di output
            output = []
            for (cod, val) in tmp:
                output.append((docs_cods[cod],val))
            
            output.sort(key=lambda tup: tup[1], reverse=True)
            guides_set = set([i for (i,j) in output])

            user_cods.pop(i)
            user_set = set(user_cods)

            le = len(user_set.intersection(guides_set))
            media += le
            print(le)
            # azzero user_cods e rimetto di nuovo tutte le chiavi dell'utente
            user_cods = user_items_dict[key]['cods'][:]
            
        
        # ordino l'output
        #output.sort(key=lambda tup: tup[1], reverse=True)
        #my_set = set([cod for (cod,val) in output[0:50]])
        
        #return my_set.intersection(set(user_items_dict[key]["cods"]))
        return media / len(user_items_dict[key]['cods'][:])
    
    '''
    get similar items
    '''
    
    def __get_similar_items(self, similar_users, name, user_items):
        # set item da non tenere in considerazione
        total_items = set()

        for (cod,desc) in user_items:
            total_items.add(cod)

        # lista dei documenti comprati dagli altri
        docs = []

        guides = ""
        for i in range(len(similar_users)):
            guides += " r.user =\'{}\' ".format(self.__users_dict[similar_users[i][0]])
            if i != len(similar_users) -1:
                guides += " OR "

        query = self.__db.execute("""
                SELECT i.codItem, i.description
                FROM items i JOIN ratings r ON i.asin = r.asin
                WHERE ({}) AND i.description IS NOT NULL
                """.format(guides))
                
        for (cod,desc) in query:
            if cod not in total_items:
                # stopword dei documenti degli altri utenti
                docs.append((cod,self.__preprocesser.stopwords_removal(desc)))
                # aggiunto per non rimetterlo dentro
                total_items.add(cod)

        output_jaccard = []
        output_tfidf = []
        output_count = []

        # prendo i 10 item dell'utente su cui fare il confronto
        ui = user_items[0:10]
        
        # scorro gli item dell'utente
        for (cod_user,desc_user) in tqdm(ui,total=10,desc="CB"):

            # stopword dell'utente
            temp = self.__preprocesser.stopwords_removal(desc_user)
            
            # metto il documento
            docs.append((cod_user,temp))

            res_jaccard = self.__item_metric_factory("jaccard", docs)[1:50]
            res_tfidf =  self.__item_metric_factory("lsi_tfidf", docs)[1:50]
            res_count =  self.__item_metric_factory("lsi_count", docs)[1:50]

            for (cod1,value) in res_jaccard:
                output_jaccard.append((docs[cod1][0],cod_user,value))
            for (cod1,value) in res_count:
                output_count.append((docs[cod1][0],cod_user,value))
            for (cod1,value) in res_tfidf:
                output_tfidf.append((docs[cod1][0],cod_user,value))

            # tolgo il documento
            docs.pop(len(docs)-1)
        output_jaccard.sort(key=lambda tup: tup[2], reverse=True)
        my_set_jaccard = [cod for (cod,cod2,value) in output_jaccard]
        
        output_count.sort(key=lambda tup: tup[2], reverse=True)
        my_set_count = [cod for (cod,cod2,value) in output_count]
        
        output_tfidf.sort(key=lambda tup: tup[2], reverse=True)
        my_set_tfidf = [cod for (cod,cod2,value) in output_tfidf]
        return (my_set_jaccard,my_set_count,my_set_tfidf)

    '''
    check wich user are similar to our user
    check items that our user didn't bought
    recommend items that are similar to the others
    '''

    def recommend_item(self, user_sim_metric, item_sim_metric, mtx, user_docs):
        similar_users = self.__get_similar_users(user_sim_metric, mtx)[1:21]
        return self.__get_similar_items(similar_users, item_sim_metric, user_docs)


    def recommend_item_with_timestamp(self, user_sim_metric, item_sim_metric, mtx, user_docs):
        similar_users = self.__get_similar_users(user_sim_metric, mtx)[1:21]
        return self.__check_timestamp_distance(similar_users, item_sim_metric, user_docs)


    def get_user_items(self) -> object:
        user_items = self.__db.execute("""
            SELECT codItem, description 
            FROM ratings r 
            JOIN items i ON r.asin=i.asin 
            WHERE r.user=\'{}\'
            """.format(self.__users_dict[self.__user]))
        return user_items
    

    def get_user_items_and_timestamp(self) -> object:
        user_items = self.__db.execute("""
            SELECT codItem, description, timestamp 
            FROM ratings r JOIN items i ON r.asin=i.asin 
            WHERE r.user=\'{}\'
            """.format(self.__users_dict[self.__user]))
        return user_items
