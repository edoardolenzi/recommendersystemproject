#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Hybrid Functions
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


from sqlalchemy import create_engine
from sqlalchemy.sql import select
from datetime import datetime
import scipy.sparse as sps
from tqdm import tqdm

import rsproject.hybrid.user_similarity_functions as us
from rsproject.content_based.text_preprocessing import TextPreprocessing


class Hybrid():


    __mtx = None
    __user = None
    __db = None
    __users_dict = {}
    __items_dict = {}
    __preprocesser = None
    __simil_user = []


    def __init__(self, user, name):
        print("Hybrid..")
        self.__user = user
        self.__mtx = self.__get_matrix()
        self.__db = self.__getDB()
        self.__preprocesser = TextPreprocessing()
        # creo il dizionario con i vari utenti
        all_users = self.__db.execute("SELECT codUser, user FROM selected_users")
        all_items = self.__db.execute("select s.codItem, asin from selected_items s join items i on s.codVecchio=i.codItem")
        for (k,v) in all_users:
            self.__users_dict[int(k)] = v
        for (k,v) in all_items:
            self.__items_dict[int(k)] = v
        self.__simil_user = self.__get_similar_users(name)[1:21]
    

    def __get_matrix(self):
        temp = sps.load_npz("matrix_filtered.npz")
        tmp_mtx = temp.tocsr()
        return tmp_mtx


    def __getDB(self):
        engine = create_engine('sqlite:///csv_database.db')
        connection = engine.connect()
        return connection

    '''
    get common users with our user
    '''

    def __get_similar_users(self, name):
        if name == "pearson":
            return us.pearson(self.__user, self.__mtx)
        if name == "proximity":
            return us.proximity_measure(self.__user, self.__mtx)
        if name == "cosine":
            return us.cosine(self.__user, self.__mtx)
        if name == "mse":
            return us.mse(self.__user, self.__mtx)
    

    def __item_metric_factory(self, name, documents):
        if name == "lsi_tfidf":
            return self.__preprocesser.lsi(documents,len(documents)-1)
        if name == "lsi_count":
            return self.__preprocesser.lsi(documents,len(documents)-1, False)
        if name == "jaccard":
            return self.__preprocesser.jaccard_list(len(documents)-1, documents)
        if name == "cosine":
            return self.__preprocesser.cosine_list(len(documents)-1, documents)
        if name == "dice":
            return self.__preprocesser.dice_list(len(documents)-1, documents)
        if name == "tdifd_cosine":
            return self.__preprocesser.tdifd_cosine(len(documents)-1, documents)


    def __get_month(self,data):
        if data.month >0 and data.month <= 3:
            month = 0
        if data.month > 3 and data.month <= 6:
            month = 1
        if data.month > 6 and data.month <= 12:
            month = 2
        return month


    '''
    check which user has bought an item in the same period
    '''
    
    def __check_timestamp_distance(self, similar_users, name):
        # calcolo item dell'user
        user_items = self.__db.execute("""
            SELECT codItem, description, timestamp 
            FROM ratings r JOIN items i ON r.asin=i.asin 
            WHERE r.user=\'{}\'
            """.format(self.__users_dict[self.__user]))
        
                # calcolo item dell'user
        already_found = set()
        user_desc = []
        for (cod, desc, time) in user_items:
            already_found.add(cod)
            user_desc.append((cod, desc, time))

        time_dict = {}

        guides = ""
        for i in range(len(similar_users)):
            guides += " r.user =\'{}\' ".format(self.__users_dict[similar_users[i][0]])
            if i != len(similar_users) -1:
                guides += " OR "

        query = self.__db.execute("""
                SELECT i.codItem, i.description, r.timestamp
                FROM items i JOIN ratings r ON i.asin = r.asin
                WHERE ({}) AND i.description IS NOT NULL
                """.format(guides))
            
        for (cod,desc,time) in query:
            # se non e gia stato trovato (quindi anche gia comprato da user)
            if cod not in already_found:
                # trasformo timestamp in date
                data = datetime.fromtimestamp(int(time))
                # calcolo il codice a partire dall'anno e dal mese (blocchi di tre mesi)
                data_temp = "{}-{}".format(data.year,self.__get_month(data))
                # se quel codice non e gia stato usato allora lo creo
                if data_temp not in time_dict:
                    time_dict[data_temp] = {}
                    # salvo la descrizione
                    time_dict[data_temp].update({"descriptions": [desc]})
                    # salvo il codice del prodotto corrispondente
                    time_dict[data_temp].update({"cods": [cod]})
                else:
                    time_dict[data_temp]["descriptions"].append(desc)
                    time_dict[data_temp]["cods"].append(cod)
                
                already_found.add(cod)

        dict_list = []
        dict_key = -1
        output = []
        # faccio per user come prima per gli utenti simili
        for (cod, desc, time) in user_desc:
            data = datetime.fromtimestamp(int(time))
            period = "{}-{}".format(data.year,self.__get_month(data))
            # vedo quale periodo ha piu acquisti da parte degli altri utenti (solo per avere piu documenti con cui confrntare)
            if period in time_dict:
                if len(time_dict[period]["descriptions"]) > len(dict_list):
                    dict_key = period
                    dict_list = time_dict[period]["descriptions"]
        print("Max value is {} in period {}".format(len(dict_list), dict_key))
        docs = [self.__preprocesser.stopwords_removal(i)  for i in dict_list]
        
        for (cod, desc, time) in user_desc:
            data = datetime.fromtimestamp(int(time))
            period = "{}-{}".format(data.year,self.__get_month(data))
            if period == dict_key:
                docs.append(self.__preprocesser.stopwords_removal(desc))
                # calcolo metrica
                print("COD USER: {}".format(cod))
                res = self.__item_metric_factory(name,docs)[1:10]
                print(res)
                # creo output
                for (cod_index,val) in res:
                    try:
                        output.append((time_dict[dict_key]["cods"][cod_index],cod,val))
                    except:
                        output.append((-1,cod,val))
                docs.pop(len(docs)-1)
        output.sort(key=lambda tup: tup[2], reverse=True)
        return output


    '''                    
    get similar items
    '''
    
    def __get_similar_items(self, similar_users, name):
        similar_items_of_users = []
        user_items = self.__db.execute("""
            SELECT codItem, description 
            FROM ratings r 
            JOIN items i ON r.asin=i.asin 
            WHERE r.user=\'{}\'
            """.format(self.__users_dict[self.__user]))
        # insieme contenente gli item gia comprati dall'utente
        user_items_set = set()
        # lista contenente i documenti gia acquistati dall'utente
        user_docs = []

        for (cod,desc) in user_items:
            user_items_set.add(cod)
            user_docs.append((cod, desc))
        # lista dei documenti comprati dagli altri
        docs = []
        # inizializzo items gia acquistati, da non reinserire nel ciclo
        total_items = set(user_items_set)

        # scorro gli utenti simili ad user
        guides = ""
        for i in range(len(similar_users)):
            guides += " r.user =\'{}\' ".format(self.__users_dict[similar_users[i][0]])
            if i != len(similar_users) -1:
                guides += " OR "
        
        query = self.__db.execute("""
                SELECT i.codItem, i.description
                FROM items i JOIN ratings r ON i.asin = r.asin
                WHERE ({}) AND i.description IS NOT NULL
                """.format(guides))

        for (cod,desc) in query:
            if cod not in total_items:
                docs.append((cod,desc))
                # aggiunto per non rimetterlo dentro
                total_items.add(cod)

        output = []
        documents = [self.__preprocesser.stopwords_removal(desc) for (cod,desc) in docs]
        leng = len(user_docs)
        for (cod_user, desc_user) in tqdm(user_docs[leng-11:leng], total=len(user_docs[leng-11:leng])):
            temp = self.__preprocesser.stopwords_removal(desc_user)
            documents.append(temp)
            res = self.__item_metric_factory(name, documents)[1:10]
            for (cod1, value) in res:
                output.append((docs[cod1][0],cod_user,value))
            documents.pop(len(documents)-1)
        output.sort(key=lambda tup: tup[2], reverse=True)
        return output[0:10]


    '''
    check wich users are similar to our user
    check items that our user didn't bought
    recommend items that are similar to the others
    '''

    def recommend_item(self, item_sim_metric) -> None:
        print("TECNINCA: {}".format(item_sim_metric))
        print(self.__get_similar_items(self.__simil_user, item_sim_metric))


    def recommend_item_with_timestamp(self, item_sim_metric) -> None:
        print(self.__check_timestamp_distance(self.__simil_user, item_sim_metric))
    