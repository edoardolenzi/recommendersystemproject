# Premesse 

Vista la grande quantità di prove e test eseguiti i file per lo studio dell'approccio ibrido non sono completamente integrati al resto del progetto (rispecchiano solamente la struttura e la nomenclatura dei campi nel DB e i nomi dei metodi/funzioni). 

Ciò è dovuto unicamente al fatto che il lavoro è stato svolto parallelamente da entrambi i membri del gruppo per velocizzare il processo di sviluppo. Inoltre, è stato necessario mantenere i file Hybrid leggermente separati dal resto del progetto per velocizzare alcune operazioni che, altrimenti, avrebbero comportato un doveroso (e oneroso in termini di tempo) update delle parti fatte in precedenza. 

I file presentano la logica ed il codice nella sua interezza, che può agilmente essere integrato con il resto del progetto.


## Guida ai file

I file utilizzati per l'approccio ibrido sono i seguenti:

* **hybrid_main**: per il lancio delle funzioni di testing e, in particolar modo, per il calcolo di recall, precision ed f1-score.
* **hybrid_functions_testing**: contentente le due funzioni che effettuano effettivamente le tecniche ibride (base e basata sui periodi di tempo, di tipo uno e tipo due) per il testing
* **hybrid_functions**: funzioni per la raccomandazione, quindi non incentrate sul testing