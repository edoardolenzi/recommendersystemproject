#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
funzioni di testing per il sistema ibrido (CF + CB), eseguibili singolarmente
'''

__author__ = 'Simone Scaboro, Edoardo Lenzi'
__version__ = '1.0'
__license__ = 'GPL-3.0'


import scipy.sparse as sps
from random import sample
from sqlalchemy import create_engine
from sqlalchemy.sql import select

from rsproject.hybrid.hybrid_functions import Hybrid
from rsproject.hybrid.hybrid_functions_testing import HybridTestingFunctions


temp = sps.load_npz('matrix_filtered.npz')
mtx = temp.tocsr()
engine = create_engine('sqlite:///csv_database.db')
connection = engine.connect()

# query provvisoria
ut = connection.execute(''' SELECT codUser, COUNT(*) as c 
                            FROM from ratings r 
                            JOIN selected_users s on r.user=s.user 
                            GROUP BY r.user having count(*) <= 1000 AND count(*)>=500 
                            ORDER BY c desc 
                            LIMIT 1000''')
lista_utenti = [cod for (cod, rat) in ut]


'''
testing ibrido base con conteggio di precision, recall e f1
'''

def testing():
    output = []
    num = 0
    truePositive = [[],[],[]]
    trueNegative = [[],[],[]]
    falsePositive = [[],[],[]]
    falseNegative = [[],[],[]]
    intersect = [[],[],[]]
    precision = [[],[],[]]
    recall = [[],[],[]]
    f1 = [[],[],[]]
    index = 0
    for USER in lista_utenti:
        h = HybridTestingFunctions(USER)
        
        user_items = h.get_user_items()
        
        # documenti dell'utente
        user_docs = [(cod, desc) for (cod,desc) in user_items]
        non_zero_elements = mtx[USER,:].nonzero()
        lenght_non_zero = len(non_zero_elements[1])
        percentage = int(lenght_non_zero * 0.2)
        tmp_list = []
        for k in range(0, 3):

            # nuova matrice
            new_matrix = mtx[:,:]

            # item dell'utente mischiati
            shuffled_user_docs = sample(user_docs, len(user_docs))

            to_azzero = []
            
            # azzero i primi 20% item
            for i in range(0, percentage):
                # prendo i primi item dell'utente
                (cod,desc) = shuffled_user_docs.pop(i)
                # azzero quell'item nella matrice
                new_matrix[USER,cod] = 0
                # 20% eliminato
                to_azzero.append(cod)
            
            # prendo gli item piu simili all'80%
            (res_jaccard, res_count, res_tfidf) = h.recommend_item('proximity', 'lsi_count', new_matrix, shuffled_user_docs)
            #res = h.recommend_item_with_timestamp('proximity', 'tfidf_count', new_matrix, shuffled_user_docs)
            my_set = set(to_azzero)
            prima_meta = []
            prima_meta.append(res_jaccard[0:int(len(res_jaccard)/2)])
            prima_meta.append(res_count[0:int(len(res_count)/2)])
            prima_meta.append(res_tfidf[0:int(len(res_tfidf)/2)])

            seconda_meta = []
            seconda_meta.append(res_jaccard[int(len(res_jaccard)/2):len(res_jaccard)])
            seconda_meta.append(res_count[int(len(res_count)/2):len(res_count)])
            seconda_meta.append(res_tfidf[int(len(res_tfidf)/2):len(res_tfidf)])
            
            intersect[0].append(len(my_set.intersection(set(res_jaccard))))
            intersect[1].append(len(my_set.intersection(set(res_count))))
            intersect[2].append(len(my_set.intersection(set(res_tfidf))))
            
            truePositive[0].append(len(my_set.intersection(set(prima_meta[0]))))
            truePositive[1].append(len(my_set.intersection(set(prima_meta[1]))))
            truePositive[2].append(len(my_set.intersection(set(prima_meta[2]))))
            
            falsePositive[0].append(len(prima_meta[0]) - len(my_set.intersection(prima_meta[0])))
            falsePositive[1].append(len(prima_meta[1]) - len(my_set.intersection(prima_meta[1])))
            falsePositive[2].append(len(prima_meta[2]) - len(my_set.intersection(prima_meta[2])))
            
            trueNegative[0].append(len(seconda_meta[0]) - len(my_set.intersection(seconda_meta[0])))
            trueNegative[1].append(len(seconda_meta[1]) - len(my_set.intersection(seconda_meta[1])))
            trueNegative[2].append(len(seconda_meta[2]) - len(my_set.intersection(seconda_meta[2])))
            
            falseNegative[0].append(len(my_set.intersection(seconda_meta[0])))
            falseNegative[1].append(len(my_set.intersection(seconda_meta[1])))
            falseNegative[2].append(len(my_set.intersection(seconda_meta[2])))

            precision[0].append(truePositive[0][index]/(truePositive[0][index]+falsePositive[0][index]) if truePositive[0][index]+falsePositive[0][index] > 0 else 0)
            precision[1].append(truePositive[1][index]/(truePositive[1][index]+falsePositive[1][index]) if truePositive[1][index]+falsePositive[1][index] > 0 else 0)
            precision[2].append(truePositive[2][index]/(truePositive[2][index]+falsePositive[2][index]) if truePositive[2][index]+falsePositive[2][index] > 0 else 0)

            recall[0].append(truePositive[0][index]/(truePositive[0][index]+falseNegative[0][index]) if truePositive[0][index]+falseNegative[0][index] else 0)
            recall[1].append(truePositive[1][index]/(truePositive[1][index]+falseNegative[1][index]) if truePositive[1][index]+falseNegative[1][index] else 0)
            recall[2].append(truePositive[2][index]/(truePositive[2][index]+falseNegative[2][index]) if truePositive[2][index]+falseNegative[2][index] else 0)
            
            f1[0].append((2*precision[0][index]*recall[0][index])/(precision[0][index]+recall[0][index]) if precision[0][index]+recall[0][index] > 0 else 0)
            f1[1].append((2*precision[1][index]*recall[1][index])/(precision[1][index]+recall[1][index]) if precision[1][index]+recall[1][index] > 0 else 0)
            f1[2].append((2*precision[2][index]*recall[2][index])/(precision[2][index]+recall[2][index]) if precision[2][index]+recall[2][index] > 0 else 0)
            index += 1

        print('INTERSECT: {}'.format(intersect))
        print('TRUE_POSITIVE: {}'.format(truePositive))
        print('FALSE_POSITIVE: {}'.format(falsePositive))
        print('TRUE_NEGATIVE: {}'.format(trueNegative))
        print('FALSE_NEGATIVE: {}'.format(falseNegative))
        print('PRECISION_JACCARD: {}'.format(sum(precision[0])/len(precision[0])))
        print('PRECISION_COUNT: {}'.format(sum(precision[1])/len(precision[1])))
        print('PRECISION_TFIDF: {}'.format(sum(precision[2])/len(precision[2])))
        
        print('RECALL_JACCARD: {}'.format(sum(recall[0])/len(recall[0])))
        print('RECALL_COUNT: {}'.format(sum(recall[1])/len(recall[1])))
        print('RECALL_TFIDF: {}'.format(sum(recall[2])/len(recall[2])))

        print('F1_JACCARD: {}'.format(sum(f1[0])/len(f1[0])))
        print('F1_COUNT: {}'.format(sum(f1[1])/len(f1[1])))
        print('F1_TFIDF: {}'.format(sum(f1[2])/len(f1[2])))


def testing2():
    USER = 7
    h = HybridTestingFunctions(USER)
    user_items = h.get_user_items_and_timestamp()
    user_docs = [(cod, desc, time) for (cod,desc, time) in user_items]
    print(h.recommend_item_with_timestamp('proximity', 'lsi_count', mtx, user_docs))


testing()
