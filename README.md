---
output:
  pdf_document: default
  html_document: default
---
# RS-Project 2018 (Recommender System Project) 

This project aims to test the techniques learned in the course 
"[*Recommender Systems*](https://elearning.uniud.it/moodle/course/info.php?id=1553)" of the professor 
[Carlo Tasso](https://users.dimi.uniud.it/~carlo.tasso/). 

We are working on an [Amazon dataset]((http://jmcauley.ucsd.edu/data/amazon/links.html)) which
contains product reviews and metadata from **Amazon**, including 1*42.8 million reviews* spanning **May 1996 - July 2014**

> The dataset is splitted in a small categorized subsets for experimentation (~1GB/category) and a raw complete dataset (100GB+).

## Rating Metadata 

```
    {
        "user": "A3R5OBKS7OM2IR",
        "item": "0000143502",
        "rating": 5.0,
        "unixReviewTime": "1358380800"
    }
```

## Review Metadata

```
    {
        "reviewerID": "A2SUAM1J3GNN3B",
        "asin": "0000013714",
        "reviewerName": "J. McDonald",
        "helpful": [2, 3],
        "reviewText": "I bought this for my husband who plays the piano...",
        "overall": 5.0,
        "summary": "Heavenly Highway Hymns",
        "unixReviewTime": 1252800000,
        "reviewTime": "09 13, 2009"
    }
```

## Product Metadata

```
    {
        "asin": "0000031852",
        "title": "Girls Ballet Tutu Zebra Hot Pink",
        "price": 3.17,
        "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
        "related":
        {
            "also_bought": ["B00JHONN1S", "B002BZX8Z6", "B00D2K1M3O", ...],
            "bought_together": ["B002BZX8Z6"]
        },
        "salesRank": {"Toys & Games": 211836},
        "brand": "Coxlures",
        "categories": [["Sports & Outdoors", "Other Sports", "Dance"]]
    }
```

## Visual Features

> *"We extracted visual features from each **product image** using a **deep CNN**. Image features are stored in a binary format, which consists of 10 characters (the **product ID**), followed by 4096 floats (repeated for every product)."*

## Categories

![Categories](Assets/categories.png)

## Credits

A special thanks to Julian McAuley for his works and availability.

* *Ups and downs: Modeling the visual evolution of fashion trends with one-class collaborative filtering* R. He, J. McAuley WWW, 2016

* *Image-based recommendations on styles and substitutes*
J. McAuley, C. Targett, J. Shi, A. van den Hengel SIGIR, 2015

The project setup follows the [Soweego project](https://github.com/Wikidata/soweego) setup. 
We have to thank the Soweego team for their excellent work.

# Pipeline

1. Repository setup
2. **Import** the dataset into a (relational) **DB**
3. Data **scraping/tidying** (with different heuristics)
4. Data **analysis** and **feasibility study**
    * Some statistics from data investigation 
5. **Baseline** creation (on reduced datasets < 1GB)
    * Scouting/training (**RS** and **ML** Python/R **libraries**)
    * Integration/implementation of some RS techniques 
    * Testing **accuracy** and **performance** of a reccommendation (with different RS techniques)  
6. **Scale** to the larger datasets (> 1GB)
   * Code **profiling** for data structures ans algorithms optimization
7. Try to **scale** to the row dataset (> 100GB)
8. Scouting of Amazon RS techniques

## RS techniques
1.  Collaborative Filtering
    1. User based
        * Find users similarities 
            * mean squared error 
            * proximity similarity
            * Person similarity coefficient  
    2. Content-based
2. Cognitive Filtering (Content based)
    * Cold start problem
    * Levenstein and Haming distance
    * Semantic similarity of unstructured data
3. LSI (Latent Semantic Indexing)
    * Clustering
    * Concepts and terms matrices
4. Content selection
    * RB (Rule Based) Filtering
        * based on user and domain knowledge
        * based on structured data (DB)
    * Association rules (hard to find a navigation  pattern)
    * Playing with support and confidence metrics

## Technologies/libraries

* Python
    * [TensorFlow](https://www.tensorflow.org/)
    * [PyTorch](https://pytorch.org/)
    * [Keras](https://keras.io/)
    * [Pandas](https://pandas.pydata.org/)
    * [Scikit-learn](https://scikit-learn.org/stable/)
    * [JellyFish](https://pypi.org/project/jellyfish/)
    * ....................
    * [SciPy](https://docs.scipy.org/doc/scipy/reference/sparse.html)
    * [SQLAlchemy](https://www.sqlalchemy.org/)
    * [Click](https://click.palletsprojects.com/en/7.x/)
    * [VirtualEnv](https://virtualenv.pypa.io/en/latest/)
      / [PipEnv](https://docs.python-guide.org/dev/virtualenvs/#installing-pipenv)
* R 
    * [tidyverse](https://cran.r-project.org/web/packages/tidyverse/index.html)
    * [ggplot2](https://cran.r-project.org/web/packages/ggplot2/index.html)
    * [ggraph](https://cran.r-project.org/web/packages/ggraph/index.html)
    * [igraph](https://cran.r-project.org/web/packages/igraph/index.html)
* Docker 
* MariaDB/PostgreSQL

## Coding style and best practices

* Comply with [PEP 8](https://www.python.org/dev/peps/pep-0008/) conventions;
* use [pylint](https://www.pylint.org/) with rules as per `.pylintrc`:
    * 4 *spaces* (soft tab) for indentation;
    * *snake-case* style, i.e., *underscore* as a word separator (files, variables, functions);
    * *UPPERCASE* constants;
    * anything else is *lowercase*;
    * 2 empty lines to separate functions;
* write [Sphinx](http://www.sphinx-doc.org/en/stable/) docstrings:
    * follow [PEP 257](https://www.python.org/dev/peps/pep-0257/) and [PEP 287](https://www.python.org/dev/peps/pep-0287/);
    * pay special attention to [field lists](http://sphinx-doc.org/domains.html#info-field-lists).

## Deadlines

* ~10/12/2018 first seminary
* end of course final presentation

# Get started

* Clone the repository and branch out of master
* Setup the [`pipenv`](https://docs.python-guide.org/dev/virtualenvs/#installing-pipenv) environment

```
    $ sudo pip install --user pipenv
```
or use version specification `python3 -m` in order to avoid version conflicts...
```
    $ sudo python3 -m pip install pipenv
```
* Install the required packages  

```
    $ sudo pipenv install -r requirements.txt
```
* enter virtualenv shell and lauch rsproject clicommand
```
    $ sudo pipenv shell
    (virtualenv)$ python3
    >>> import nltk
    >>> nltk.download('stopwords')
    >>> quit()
    (virtualenv)$ python3 -m rsproject
```
* in the `rsproject/` create the following folders:
  * `./rsproject/resources/chunk`
  * `./rsproject/resources/raw`
  * `./rsproject/resources/output`
  * `./rsproject/resources/temp`

Downloads and extract into the `./rsproject/resources/raw` folder 
the datasets

Once the virtualenvironment is enabled the list of available commands 
is availble with the following comman:

```[sh]
    python -m rsproject
```