\select@language {english}
\contentsline {chapter}{\numberline {1}Overview}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Pipeline del progetto}{3}{section.1.1}
\contentsline {chapter}{\numberline {2}Dataset}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Scelta del Dataset}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Scalabilit\IeC {\`a} del dataset}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Formato del dataset}{5}{section.2.3}
\contentsline {section}{\numberline {2.4}Metadati disponibili}{6}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Metadati dei prodotti}{6}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Metadati delle recensioni degli utenti}{6}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Metadati delle votazioni degli utenti}{6}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Principali Entit\IeC {\`a} importate}{7}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Statistiche del dataset}{7}{section.2.5}
\contentsline {subsection}{\numberline {2.5.1}Librerie utilizzate}{7}{subsection.2.5.1}
\contentsline {subsubsection}{Python}{7}{subsubsection*.3}
\contentsline {subsubsection}{R}{8}{subsubsection*.4}
\contentsline {chapter}{\numberline {3}Data analysis}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Long Tail}{8}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Long tail dei prodotti}{8}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Long tail degli utenti}{9}{subsection.3.1.2}
\contentsline {subsection}{\numberline {3.1.3}Long tail della popolarit\IeC {\`a} dei prodotti}{10}{subsection.3.1.3}
\contentsline {section}{\numberline {3.2}Data tidying}{11}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Data Tidying - Database}{11}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Filtraggio dei dati - Eliminazione di utenti}{11}{subsection.3.2.2}
\contentsline {chapter}{\numberline {4}Techniques}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Matrix}{11}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Creazione della matrice}{11}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Librerie per la gestione della matrice}{12}{subsection.4.1.2}
\contentsline {section}{\numberline {4.2}Collaborative filtering}{12}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Metriche e tecniche di filtraggio collaborativo}{12}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Alcuni risultati}{13}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Mean Squared Error}{13}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Analisi del risultato}{13}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Considerazione MSE}{14}{subsection.4.2.5}
\contentsline {subsection}{\numberline {4.2.6}Computazione di MSE}{14}{subsection.4.2.6}
\contentsline {subsection}{\numberline {4.2.7}Proximity measure}{14}{subsection.4.2.7}
\contentsline {subsection}{\numberline {4.2.8}Considerazioni Proximity}{14}{subsection.4.2.8}
\contentsline {subsection}{\numberline {4.2.9}Computazione di Proximity Similarity}{15}{subsection.4.2.9}
\contentsline {subsection}{\numberline {4.2.10}Pearson Similarity}{15}{subsection.4.2.10}
\contentsline {subsection}{\numberline {4.2.11}Computazione di Pearson Similarity}{15}{subsection.4.2.11}
\contentsline {subsection}{\numberline {4.2.12}Cosine similarity per gli utenti}{16}{subsection.4.2.12}
\contentsline {subsection}{\numberline {4.2.13}Computazione di Cosine Similarity}{16}{subsection.4.2.13}
\contentsline {subsection}{\numberline {4.2.14}Considerazioni sugli indici: Cosine e Pearson}{16}{subsection.4.2.14}
\contentsline {section}{\numberline {4.3}Predictions}{16}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}User based Predictions}{16}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Fasi di test}{17}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Predizioni risultanti da varie tecniche}{17}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Future works}{17}{chapter.5}
\contentsline {section}{\numberline {5.1}Next sprint scheduling}{17}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Futuri sviluppi}{17}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Futuri sviluppi}{18}{subsection.5.1.2}
\contentsline {subsection}{\numberline {5.1.3}Che operazioni stiamo svolgendo ora?}{18}{subsection.5.1.3}
\contentsline {subsection}{\numberline {5.1.4}Esempio di stopword removal}{19}{subsection.5.1.4}
\contentsline {subsection}{\numberline {5.1.5}Futuri sviluppi}{19}{subsection.5.1.5}
\contentsline {subsection}{\numberline {5.1.6}Futuri sviluppi}{19}{subsection.5.1.6}
\contentsline {subsection}{\numberline {5.1.7}Futuri sviluppi}{19}{subsection.5.1.7}
\contentsline {section}{\numberline {5.2}Impediments}{19}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Problematiche riscontrate}{19}{subsection.5.2.1}
\contentsline {chapter}{Bibliografia}{20}{Item.23}
