# Seminar 

From the links of the project documentation we reach the project: 
**Amazon product data** edited by Julian McAuley, UCSD. 
The repository contains the Amazon datasets classified by product 
macro-categories. Only reduced datasets for experimentation 
are exposed (~1-3 GB) but, thanks to Julian we also get the 
links for the original row dataset (> 100GB).

The general idea is to start scraping/computing a single category 
of the reduced dataset and build a strong baseline on it.
Next try to impoove the recommendations using different techniques
and hybrid techniques (i.e. for cold    start problem).
Finally the dream is to optimize the code in order to work on 
the original row dataset but this is a great challenge for the 
dump dimensions. 

More deeper we start from the category *"Movies and TV"*, 
this category has 3 main logical entities which are:

| Table     | Size      | # Entities    |
| --------- |:---------:| -------------:|
| products  | 270 MB    | 208.321       |  
| reviews   | 3.9 GB    | 4,607,047     |
| ratings   | 185 MB    | 4.607.047     |

## Product Metadata

```
    {
        "asin": "0000031852",
        "title": "Girls Ballet Tutu Zebra Hot Pink",
        'description': '3Pack DVD set ...",
        "price": 3.17,
        "imUrl": "http://ecx.images-amazon.com/images/I/51fAmVkTbyL._SY300_.jpg",
        "related":
        {
            "also_bought": ["B00JHONN1S", "B002BZX8Z6", "B00D2K1M3O", ...],
            "bought_together": ["B002BZX8Z6"]
        },
        "salesRank": {"Toys & Games": 211836},
        "brand": "Coxlures",
        "categories": [["Sports & Outdoors", "Other Sports", "Dance"]]
    }
```

## Review Metadata

```
    {
        "reviewerID": "A2SUAM1J3GNN3B",
        "asin": "0000013714",
        "reviewerName": "J. McDonald",
        "helpful": [2, 3],
        "reviewText": "I bought this for my husband who plays the piano...",
        "overall": 5.0,
        "summary": "Heavenly Highway Hymns",
        "unixReviewTime": 1252800000,
        "reviewTime": "09 13, 2009"
    }
```

## Rating Metadata 

```
    {
        "user": "A3R5OBKS7OM2IR",
        "item": "0000143502",
        "rating": 5.0,
        "unixReviewTime": "1358380800"
    }
```

![UML](Images/uml.png)

Why continue to use Ratings? cause it is lightweight
(185 MB vs 3.9 GB)! 

# Data analysis and statistics measures

## Counters
* Number of products: 208.321
* Number of users: 2.088.620
    * Number of considered users (for training): 46.483
* Number of ratings: 4.607.047
    * Number of considered rating (for training): 1.349.351
  
## Means
* Mean of the ratings per user: 4.14
* Mean of the reviews per user: 29.02
* Mean variace: 0.004


# Todo 
* End before weekend in order to send everything to Tasso for a review 
* Interestig a comparison between different categories

# R code
install.packages("RSQLite")
install.packages("devtools")
devtools::install_github("rstats-db/RSQLite")
