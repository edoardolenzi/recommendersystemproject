\select@language {english}
\beamer@sectionintoc {1}{Overview}{3}{0}{1}
\beamer@subsectionintoc {1}{1}{Work Scheduling}{3}{0}{1}
\beamer@sectionintoc {2}{Tools $\&$ Datasets}{4}{0}{2}
\beamer@subsectionintoc {2}{1}{Dataset Choice}{4}{0}{2}
\beamer@subsectionintoc {2}{2}{Metadata}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Categories}{11}{0}{2}
\beamer@subsectionintoc {2}{4}{Metrics $\&$ indices}{13}{0}{2}
\beamer@sectionintoc {3}{Data analysis}{16}{0}{3}
\beamer@subsectionintoc {3}{1}{Long Tail}{16}{0}{3}
\beamer@subsectionintoc {3}{2}{Data tidying}{19}{0}{3}
\beamer@sectionintoc {4}{Techniques}{21}{0}{4}
\beamer@subsectionintoc {4}{1}{User-Item Matrix}{21}{0}{4}
\beamer@subsectionintoc {4}{2}{Collaborative filtering}{23}{0}{4}
\beamer@subsectionintoc {4}{3}{Mean Squared Error}{25}{0}{4}
\beamer@subsectionintoc {4}{4}{Proximity measure}{28}{0}{4}
\beamer@subsectionintoc {4}{5}{Pearson Similarity}{31}{0}{4}
\beamer@subsectionintoc {4}{6}{Cosine Similarity}{33}{0}{4}
\beamer@subsectionintoc {4}{7}{Predictions}{38}{0}{4}
\beamer@sectionintoc {5}{Future works}{42}{0}{5}
\beamer@subsectionintoc {5}{1}{Next sprint scheduling}{42}{0}{5}
\beamer@subsectionintoc {5}{2}{Impediments}{49}{0}{5}
\beamer@subsectionintoc {5}{3}{Notions}{50}{0}{5}
