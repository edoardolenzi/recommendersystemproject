# Tipologia: SPERIMENTALE 

> Verificare come su un data set (tipicamente matrice user – ranking degli item) 
alcune tecniche selezionate di Filtraggio 
(tra cui tecniche di ML, probablmente si tratterà di Filtr. Collaborativo o Ibrido) 
portano a risultati a diversi peggiori o migliori.

# Step del progetto: 
1. Individuare Strumenti e *Data Set candidati*; 
2. Sperimentare, con le *diverse tecniche* esaminate nel corso e *diversi* data set, 
    al fine di individuare un metodo di lavoro; 
3. *Selezionare le tecniche* (ed i relativi tool) 
    e il data set su cui operare la sperimentazione; 
4. Raccogliere i *dati di performance* per ciascuna tecnica utilizzata; 
5. Valutare/perfezionare la sperimentazione e costruire una presentazione conclusiva.
    Materiali: Data Set (User – Ranking); 
