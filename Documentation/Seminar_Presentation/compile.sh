#!/bin/bash

rm *.aux
rm *.out
rm *.toc
rm *.nav 
rm *.snm
rm *.vrb
rm *.bbl
rm *.blg
pdflatex *.tex