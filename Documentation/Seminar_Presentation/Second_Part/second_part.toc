\select@language {english}
\beamer@sectionintoc {1}{Updates}{4}{0}{1}
\beamer@subsectionintoc {1}{1}{Updates}{4}{0}{1}
\beamer@sectionintoc {2}{Content based}{9}{0}{2}
\beamer@subsectionintoc {2}{1}{Lineup of the speech}{9}{0}{2}
\beamer@subsectionintoc {2}{2}{Tools and libraries}{10}{0}{2}
\beamer@subsectionintoc {2}{3}{Metadata}{11}{0}{2}
\beamer@subsectionintoc {2}{4}{Items descriptions}{12}{0}{2}
\beamer@sectionintoc {3}{Preprocessing}{13}{0}{3}
\beamer@subsectionintoc {3}{1}{Stopwords removal}{13}{0}{3}
\beamer@subsectionintoc {3}{2}{Data}{15}{0}{3}
\beamer@subsectionintoc {3}{3}{Stemming}{16}{0}{3}
\beamer@subsectionintoc {3}{4}{Bag of words}{18}{0}{3}
\beamer@sectionintoc {4}{Techniques}{19}{0}{4}
\beamer@subsectionintoc {4}{1}{Jaccard, Cosine e Dice}{19}{0}{4}
\beamer@subsectionintoc {4}{2}{Jaccard}{22}{0}{4}
\beamer@subsectionintoc {4}{3}{Dice}{23}{0}{4}
\beamer@subsectionintoc {4}{4}{Tf*idf}{24}{0}{4}
\beamer@subsectionintoc {4}{5}{Tfidf Conclusions}{28}{0}{4}
\beamer@subsectionintoc {4}{6}{LSI}{29}{0}{4}
\beamer@subsectionintoc {4}{7}{LSI - Results}{33}{0}{4}
\beamer@subsectionintoc {4}{8}{LSI VS Tf*idf}{36}{0}{4}
\beamer@sectionintoc {5}{Hybrid}{37}{0}{5}
\beamer@subsectionintoc {5}{1}{Hybrid Approaches}{37}{0}{5}
\beamer@subsectionintoc {5}{2}{Naive Technique}{39}{0}{5}
\beamer@subsectionintoc {5}{3}{First Hybrid Technique}{47}{0}{5}
\beamer@subsectionintoc {5}{4}{Second Hybrid Technique}{50}{0}{5}
\beamer@sectionintoc {6}{Probabilistic Techniques}{52}{0}{6}
\beamer@subsectionintoc {6}{1}{Naive Bayes}{52}{0}{6}
\beamer@subsectionintoc {6}{2}{Bayesian Network}{53}{0}{6}
\beamer@subsectionintoc {6}{3}{Clustering}{55}{0}{6}
