# R on big data

## Launch a query

```{R}
  library("dplyr")
  library("RSQLite")
  runsql <- function(sql, dbname="csv_database.db"){
    require(RSQLite)
    driver <- dbDriver("SQLite")
    connect <- dbConnect(driver, dbname=dbname);
    closeup <- function(){
      sqliteCloseConnection(connect)
      sqliteCloseDriver(driver)
    }
    dd <- tryCatch(dbGetQuery(connect, sql), finally=closeup)
    return(dd)
  }
```

## Plot item long-tail 
```{R}
  library("ggplot2")

  results <- runsql("select count(*) as counter from rating group by item order by counter desc")
  results <- mutate(results, id = c(1:nrow(results)))
  results

  item_long_tail <- ggplot(data=results) +
    geom_point(mapping = aes(y = counter, x = id)) + 
    labs(x = "Prodotto", y = "Vendite totali")

  
  savepng <- function(file_name, my_plot, dbname="csv_database.db"){
    file_path <- "Seminar_Presentation/Images"
    ggsave(paste(file_path, file_name, sep = "/"), plot = my_plot, device = NULL, path = NULL,
    scale = 1, width = NA, height = NA, units = c("in", "cm", "mm"),
    dpi = 300, limitsize = TRUE)
  }
  
  item_long_tail
  savepng("item_long_tail.png", item_long_tail)
  
  item_long_tail_2 = item_long_tail + coord_cartesian(ylim=c(0,100), xlim=c(0,100000))
  item_long_tail_2
  savepng("item_long_tail_2.png", item_long_tail_2)
  
```

```{R}
  results_2 <- runsql("select count(*) as counter from rating group by user order by counter desc")
  results_2 <- mutate(results_2, id = c(1:nrow(results_2)))
  user_long_tail <- ggplot(data=results_2) +
    geom_point(mapping = aes(y = counter, x = id)) + 
    labs(x = "Utente", y = "Acquisti totali")  
  user_long_tail
  savepng("user_long_tail.png", user_long_tail)
  
  user_long_tail_2 = user_long_tail + coord_cartesian(ylim=c(0,500), xlim=c(0,100000))
  user_long_tail_2
  savepng("user_long_tail_2.png", user_long_tail_2)
```

```{r}
numrighe = nrow(results)
numrighe
ventipercento = (20/100) * numrighe
ventipercento

total = sum(as.numeric(unlist(results$counter)), na.rm = FALSE)
first = sum(as.numeric(unlist(head(results$counter, ventipercento))), na.rm = FALSE)
(first * 100 )/total 


numrighe = nrow(results_2)
numrighe
ventipercento = (20/100) * numrighe
ventipercento

total = sum(as.numeric(unlist(results_2$counter)), na.rm = FALSE)
first = sum(as.numeric(unlist(head(results_2$counter, ventipercento))), na.rm = FALSE)
(first * 100 )/total 
```

```{r}
  results_3 <- runsql("select rating, count(*) as counter from rating group by rating order by counter desc")
  results_3 <- mutate(results_3, id = c(1:nrow(results_3)))
  rating_long_tail <- ggplot(data=results_3) +
    geom_point(mapping = aes(y = id, x = counter)) + 
    geom_line(aes(y = id, x = counter)) +
    labs(x = "Totale Prodotti", y = "Votazione")
  
  rating_long_tail
  savepng("rating_long_tail.png", rating_long_tail)
  
  
  numrighe = nrow(results_3)
  numrighe
  ventipercento = (20/100) * numrighe
  ventipercento
  
  total = sum(as.numeric(unlist(results_3$counter)), na.rm = FALSE)
  first = sum(as.numeric(unlist(head(results_3$counter, ventipercento))), na.rm = FALSE)
  (first * 100 )/total 
```