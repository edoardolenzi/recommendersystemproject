# Notes


## Dependencies


* update your libraries
```
    $ pip install pur
    $ pur -r requirements.txt
```


* freeze your local dependencies
```
    $ sudo pip freeze > requirements.txt
```


# Features


## Start PipEnv 


```{python}
    python3 -m pipenv shell
```


## Import Datasets


```{python}
    python -m rsproject import
```