import asyncio  
import time  
from datetime import datetime
from time import sleep
from concurrent.futures import ProcessPoolExecutor
from rsproject.decorators.async import force_async
from rsproject.bayesian_network.bayesian_network_example import BNExample
from rsproject.bayesian_network.migration import Migration

#Migration().tfidf()


import click
import os

from rsproject.importer.importer import Importer
from rsproject.commons.sparse_matrix import SparseMatrix
from rsproject.commons.models.matrix import Matrix
from rsproject.similarities.cosine_similarity import CosineSimilarity
from rsproject.similarities.mse import MSE
from rsproject.similarities.pearson_similarity import PearsonSimilarity
from rsproject.similarities.mean_adjusted_cosine_similarity import MeanAdjustedCosineSimilarity
from rsproject.similarities.proximity_measure import ProximityMeasure
from rsproject.statistics.statistics import Statistics
from rsproject.statistics.notes import Notes
import rsproject.commons.constants as const
from rsproject.testing.cf_testing import CfTesting
from rsproject.testing.cb_testing import CbTesting
from rsproject.bayesian_network.migration import Migration

from rsproject.commons.db_manager import DBManager

test = CfTesting(int(const.env('P_USER1_INDEX')), Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
test.evaluation_item_based() 









db = DBManager()
a = db.execute('select u.user from selected_users su, users u where su.user = u.id and su.id = {0}'.format(5))
b = [x for x in a][0][0]




#test = CfTesting(71528, Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
#test.test('cosine') 
ratings = db.execute('''
select rating from ratings
''')

avg = 4.28
sum = 0
for (rating) in ratings:
    sum += (float(rating._row[0]) - avg)**2 
variance = sum / 4607047
#1.5291



ratings = db.execute('''
select rating from selected_ratings1
''')

avg = 4.16 
sum = 0
for (rating) in ratings:
    sum += (float(rating._row[0]) - avg)**2 
variance = sum / 3142335
#1.4593


pearson = MSE(Matrix(None, const.SPARSE_FILTERED_MATRIX_PATH))
data = pearson.execute(70537)


#print('simo {0}'.format(sim.adjusted_cosine_similarity(47788, 36579)))


#a = 0
#
#async def sleep_async():
#    print('SLEEP', datetime.now())
#    global a 
#    a += 4
#    await asyncio.sleep(1)
#
#@force_async
#def custom_sleep():  
#    print('SLEEP', datetime.now())
#    global a 
#    a += 5
#    time.sleep(1)
#
#start = time.time()  
#loop = asyncio.get_event_loop()
#
#tasks = [  
#    asyncio.ensure_future(custom_sleep()),
#    asyncio.ensure_future(custom_sleep()),
#    asyncio.ensure_future(custom_sleep()),
#    asyncio.ensure_future(custom_sleep()),
#    asyncio.ensure_future(sleep_async()),
#    asyncio.ensure_future(sleep_async()),
#    asyncio.ensure_future(sleep_async()),
#]
#
#loop.run_until_complete(asyncio.wait(tasks))  
#loop.close()
#end = time.time()  
#
#print(a)
#print("Total time: {}".format(end - start))


#from pomegranate import *
#
#guest = DiscreteDistribution({'A': 1./3, 'B': 1./3, 'C': 1./3})
#prize = DiscreteDistribution({'A': 1./3, 'B': 1./3, 'C': 1./3})
#monty = ConditionalProbabilityTable(
#        [['A', 'A', 'A', 0.0],
#         ['A', 'A', 'B', 0.5],
#         ['A', 'A', 'C', 0.5],
#         ['A', 'B', 'A', 0.0],
#         ['A', 'B', 'B', 0.0],
#         ['A', 'B', 'C', 1.0],
#         ['A', 'C', 'A', 0.0],
#         ['A', 'C', 'B', 1.0],
#         ['A', 'C', 'C', 0.0],
#         ['B', 'A', 'A', 0.0],
#         ['B', 'A', 'B', 0.0],
#         ['B', 'A', 'C', 1.0],
#         ['B', 'B', 'A', 0.5],
#         ['B', 'B', 'B', 0.0],
#         ['B', 'B', 'C', 0.5],
#         ['B', 'C', 'A', 1.0],
#         ['B', 'C', 'B', 0.0],
#         ['B', 'C', 'C', 0.0],
#         ['C', 'A', 'A', 0.0],
#         ['C', 'A', 'B', 1.0],
#         ['C', 'A', 'C', 0.0],
#         ['C', 'B', 'A', 1.0],
#         ['C', 'B', 'B', 0.0],
#         ['C', 'B', 'C', 0.0],
#         ['C', 'C', 'A', 0.5],
#         ['C', 'C', 'B', 0.5],
#         ['C', 'C', 'C', 0.0]], [guest, prize])
#
#s1 = Node(guest, name="guest")
#s2 = Node(prize, name="prize")
#s3 = Node(monty, name="monty")
#
#model = BayesianNetwork("Monty Hall Problem")
#model.add_states(s1, s2, s3)
#model.add_edge(s1, s3)
#model.add_edge(s2, s3)
#model.bake()

